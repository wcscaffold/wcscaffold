using UnityEngine;
using System.Collections;

[RequireComponent( typeof( Camera ) )]
[RequireComponent( typeof( PinchRecognizer ) )]
public class TBPinchZoom : MonoBehaviour
{
    public float zoomSpeed = 5.0f;
    public float minZoomAmount = 0;
    public float maxZoomAmount = 50;
    public float SmoothSpeed = 4.0f;

    Vector3 defaultPos = Vector3.zero;
    float idealZoomAmount = 0;
    float zoomAmount = 0;

    public float IdealZoomAmount
    {
        get {return idealZoomAmount;}
        set {idealZoomAmount = Mathf.Clamp(value, minZoomAmount, maxZoomAmount);}
    }

    public float ZoomAmount
    {
        get { return zoomAmount; }
        set
        {
            zoomAmount = Mathf.Clamp( value, minZoomAmount, maxZoomAmount ); 
            transform.position = defaultPos + zoomAmount * transform.forward;                 
        }
    }

    void Start()
    {
        defaultPos = transform.position;
    }

    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            ZoomAmount = Mathf.Lerp(ZoomAmount, IdealZoomAmount, Time.deltaTime * SmoothSpeed);
        }
    }

    void OnPinch( PinchGesture gesture )
    {
        IdealZoomAmount += zoomSpeed * gesture.Delta.Centimeters();
    }
}
