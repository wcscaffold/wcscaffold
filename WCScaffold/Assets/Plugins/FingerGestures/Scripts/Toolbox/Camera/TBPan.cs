using UnityEngine;
using System.Collections;

public class TBPan : MonoBehaviour
{
    Transform cachedTransform;

    public float sensitivity = 1.0f;
    public float smoothSpeed = 10;
    public BoxCollider moveArea;    

    Vector3 idealPos;
    DragGesture dragGesture;

    public delegate void PanEventHandler( TBPan source, Vector3 move );
    public event PanEventHandler OnPan;

    void Awake()
    {
        cachedTransform = this.transform;
    }

    void Start()
    {
        idealPos = cachedTransform.position;
    }

    void OnDrag(DragGesture gesture)
    {
        dragGesture = (gesture.State == GestureRecognitionState.Ended) ? null : gesture;
        print(gesture);
    }

    void Update()
    {
        if (dragGesture != null)
        {
            if (dragGesture.DeltaMove.SqrMagnitude() > 0)
            {
                Vector2 screenSpaceMove = sensitivity * dragGesture.DeltaMove;
                Vector3 worldSpaceMove = screenSpaceMove.x * cachedTransform.right + screenSpaceMove.y * cachedTransform.up;
                idealPos -= worldSpaceMove;

                if (OnPan != null)
                    OnPan(this, worldSpaceMove);
            }
        }
        idealPos = ConstrainToMoveArea(idealPos);
        cachedTransform.position = Vector3.Lerp(cachedTransform.position, idealPos, Time.deltaTime * smoothSpeed);
    }

    public Vector3 ConstrainToMoveArea(Vector3 p)
    {
        if (moveArea)
        {
            Vector3 min = moveArea.bounds.min;
            Vector3 max = moveArea.bounds.max;

            p.x = Mathf.Clamp(p.x, min.x, max.x);
            p.y = Mathf.Clamp(p.y, min.y, max.y);
            p.z = Mathf.Clamp(p.z, min.z, max.z);
        }
        return p;
    }
}
