// FingerGestures copyright (c) William Ravaine

using UnityEngine;
using System.Collections;

[RequireComponent( typeof( DragRecognizer ) )]
public class TBDragView : MonoBehaviour
{
    public bool allowUserInput = true;  // set this to false to prevent the user from dragging the view
    public float sensitivity = 360.0f;
    public float dragAcceleration = 40.0f;
    public float dragDeceleration = 15.0f;
    public float minPitchAngle = -60.0f;
    public float maxPitchAngle = 60.0f;
    public float idealRotationSmoothingSpeed = 7.0f; // set to 0 to disable smoothing when rotating toward ideal direction

    float _direct = 1;

    Transform cachedTransform;
    Vector2 angularVelocity = Vector2.zero;
    Quaternion idealRotation;

    DragGesture dragGesture;

    void Awake()
    {
        cachedTransform = this.transform;
    }

    void Start()
    {
        IdealRotation = cachedTransform.rotation;
    }

    public bool Dragging
    {
        get { return dragGesture != null; }
    }

    void OnDrag(DragGesture gesture)
    {
        if (gesture.Phase != ContinuousGesturePhase.Ended)
            dragGesture = gesture;
        else
            dragGesture = null;
    }

    void Update()
    {
        Vector3 localAngles = transform.localEulerAngles;
        Vector2 idealAngularVelocity = Vector2.zero;

        float accel = dragDeceleration;

        if( Dragging )
        {
            idealAngularVelocity = sensitivity * dragGesture.DeltaMove.Centimeters();
            accel = dragAcceleration;
        }
        angularVelocity = Vector2.Lerp( angularVelocity, idealAngularVelocity, Time.deltaTime * accel );
        Vector2 angularMove = Time.deltaTime * -angularVelocity * _direct;

        // pitch angle
        localAngles.x = Mathf.Clamp( NormalizePitch( localAngles.x + angularMove.y ), minPitchAngle, maxPitchAngle );
        // yaw angle
        localAngles.y -= angularMove.x;
        // apply
        transform.localEulerAngles = localAngles;    
    }

    static float NormalizePitch( float angle )
    {
        if( angle > 180.0f )
            angle -= 360.0f;
        return angle;
    }

    public Quaternion IdealRotation
    {
        get { return idealRotation; }
        set { idealRotation = value; }
    }
}
