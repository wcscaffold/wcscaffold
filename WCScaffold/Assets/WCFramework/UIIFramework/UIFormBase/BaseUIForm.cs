﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;
public class BaseUIForm : MonoBehaviour, IController
{
    private UIType uiType;
    protected IUIManagerSystem iuIManagerSystem;
    public UIType CurrentUIType
    {
        set { uiType = value; }
        get { return uiType; }
    }

    protected virtual void Init()
    {

    }

    protected virtual void Init(UIFormShowMode uIFormShowMode, UIFormType uIFormType, UIFormLucencyType uIFormLucencyTyp = UIFormLucencyType.Lucency)
    {
        iuIManagerSystem = this.GetSystem<IUIManagerSystem>();
        uiType = new UIType();
        CurrentUIType.UIForms_ShowMode = uIFormShowMode;
        CurrentUIType.UIForms_Type = uIFormType;
        CurrentUIType.UIForms_LucencyType = uIFormLucencyTyp;
        Init();
    }

    /// <summary>
    /// 页面显示
    /// </summary>
    public virtual void Display()
    {
        this.gameObject.SetActive(true);

        //设置模态窗体调用(必须是弹出窗体)
        if (CurrentUIType.UIForms_Type == UIFormType.PopUp)
        {
            this.GetSystem<IUIMaskMgrSystem>().SetMaskWindow(this.gameObject, CurrentUIType.UIForms_LucencyType);
        }
    }

    /// <summary>
    /// 页面隐藏
    /// </summary>
    public virtual void Hiding()
    {
        this.gameObject.SetActive(false);
        //取消模态窗体调用
        if (CurrentUIType.UIForms_Type == UIFormType.PopUp)
        {
            this.GetSystem<IUIMaskMgrSystem>().CancelMaskWindow();
        }
    }

    /// <summary>
    /// 页面重新显示
    /// </summary>
    public virtual void ReDisplay()
    {
        this.gameObject.SetActive(true);

        //设置模态窗体调用(必须是弹出窗体)
        if (CurrentUIType.UIForms_Type == UIFormType.PopUp)
        {
            this.GetSystem<IUIMaskMgrSystem>().SetMaskWindow(this.gameObject, CurrentUIType.UIForms_LucencyType);
        }
    }

    /// <summary>
    /// 页面冻结（还在栈中）
    /// </summary>
    public virtual void Freeze()
    {
        this.gameObject.SetActive(true);
    }

    /// <summary>
    /// 关闭某个UI窗体
    /// </summary>
    protected void CloseUIForm<T>() where T : BaseUIForm
    {
        iuIManagerSystem.CloseUIForm<T>();
    }

    /// <summary>
    /// 关闭某个UI窗体
    /// </summary>
    protected void CloseUIForm<T>(GameObject gameObject, bool isOpen) where T : BaseUIForm
    {
        if (isOpen)
        {
            iuIManagerSystem.CloseUIForm<T>();
        }
    }


    /// <summary>
    /// 打开UI窗体
    /// </summary>
    protected void OpenUIForm<T>(SceneName sceneName) where T : BaseUIForm
    {
        iuIManagerSystem.ShowUIForm<T>(sceneName);
    }

    public virtual void SetVideo(string videoName, string timer)
    {
        this.gameObject.SetActive(true);
    }

    public IArchitecture GetArchitecture()
    {
        return GameCneter.Interface;
    }
}
