﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using FrameworkDesign;

public enum ButtonType
{
    Normal,
    /// <summary>
    /// 重置按钮,点击后按钮组中所有的按钮全部取消选中
    /// </summary>
    Rest,
    /// <summary>
    /// 状态按钮,不参与按钮组中的只能选中一个的逻辑
    /// 参与重置按钮全部取消选中的逻辑
    /// </summary>
    State
}
public enum WCButtonType
{
    None,
    CommonImageButton,
    CommonTextButton,
    TextAndImageButton,
    AllColorButton
}
public class WCBaseButton : MonoBehaviour, IController, IPointerDownHandler, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] public WCButtonType wcButtonType = WCButtonType.None;

    [Tooltip("按钮图片")] [SerializeField] public Image buttonImage;
    [Tooltip("普通状态下的按钮图片")] [SerializeField] public Sprite normalTexture;
    [Tooltip("选中状态下的按钮图片")] [SerializeField] public Sprite selectTexture;
    [Tooltip("悬停时替换的图片")] [SerializeField] public Sprite hoverTexture;
    [Tooltip("鼠标按下状态时替换的图片")] [SerializeField] public Sprite pressTexture;

    [Tooltip("按钮文字")] [SerializeField] public Text buttonText;

    [Tooltip("普通状态下的文字或图片的颜色")] [SerializeField] public Color normalColor = new Color(1, 1, 1, 1);
    [Tooltip("选中状态下的文字或图片的颜色")] [SerializeField] public Color selectColor = new Color(0.95f, 0.95f, 0.95f, 1);
    [Tooltip("悬停状态下的文字或图片的颜色")] [SerializeField] public Color hoverColor = new Color(0.95f, 0.95f, 0.95f, 1);
    [Tooltip("鼠标按下状态下的文字或图片的颜色")] [SerializeField] public Color pressColor = new Color(0.8f, 0.8f, 0.8f, 1);

    [Tooltip("普通状态图片的颜色")] [SerializeField] public Color imageNormalColor = new Color(1, 1, 1, 1);
    [Tooltip("选中状态图片的颜色")] [SerializeField] public Color imageSelectColor = new Color(0.95f, 0.95f, 0.95f, 1);
    [Tooltip("悬停状态图片的颜色")] [SerializeField] public Color imageHoverColor = new Color(0.95f, 0.95f, 0.95f, 1);
    [Tooltip("鼠标按下状态下图片的颜色")] [SerializeField] public Color imagePressColor = new Color(0.8f, 0.8f, 0.8f, 1);


    [Tooltip("按钮组")] public ButtonGroup buttonGroup;
    [Tooltip("按钮组类型")] public ButtonType buttonGroupType;


    [Tooltip("鼠标悬停的时候会有信息提示")] [SerializeField] public GameObject tipsObj;

    [Tooltip("在纯文字情况下选中后有图片显示")] [SerializeField] public GameObject selectImageObj;


    [SerializeField] public bool interactable = true;
    [SerializeField] public bool isCanRepeatClick = true;
    [SerializeField] public bool isCanSelect = true;

    [SerializeField] public bool isOn = false;


    /// <summary>
    /// 隐藏按钮时 是否需要还原（在一些根据服务器数据来显示 是否选中状态的按钮 隐藏后是不需要还原的）
    /// </summary>
    public bool isNeddReduction = true;

    public SoundName buttonSoundName = SoundName.ButtonGeneral;
    public float soundVolume = 1;

    /// <summary>
    /// 按钮点击时触发的事件
    /// </summary>
    [HideInInspector] public Action<GameObject, bool> onValueChanged;


    private void Awake()
    {
        if (buttonGroup != null)
        {
            switch (buttonGroupType)
            {
                case ButtonType.Normal:
                    buttonGroup.GroupAddNoramlButton(this);
                    break;
                case ButtonType.State:
                    buttonGroup.GroupAddStateButton(this);
                    break;
                case ButtonType.Rest:
                    buttonGroup.RestSelfTypeButton = this;
                    break;
            }
        }
    }

    public void AddButtonGroup(ButtonGroup buttonGroup)
    {
        this.buttonGroup = buttonGroup;
    }

    /// <summary>
    /// 按钮是否选中
    /// </summary>
    public bool IsOn
    {
        get { return isOn; }
        set
        {
            ChangeIsOn(value, true);
        }
    }

    /// <summary>
    /// 不带事件的选中
    /// </summary>
    public bool IsOnNoChangeAction
    {
        get { return isOn; }
        set
        {
            ChangeIsOn(value, false);
        }
    }

    private void ChangeIsOn(bool value, bool haveAction)
    {
        if (isOn != value)
        {
            isOn = value;
            if (isOn && !isCanRepeatClick)
            {
                interactable = false;
            }
            else
            {
                interactable = true;
            }

            ExecuteValueChanged(haveAction);
            if (!isCanSelect)
            {
                isOn = false;
            }
        }
    }

    private void OnDisable()
    {
        if (isNeddReduction)
        {
            IsOn = false;
        }
    }

    private void ExecuteValueChanged(bool isNeedAction)
    {
        if (isNeedAction)
        {
            onValueChanged?.Invoke(this.gameObject, IsOn);
        }

        if (IsOn)
        {
            IsOnTrue();
        }
        else
        {
            IsOnFalse();
        }
        if (buttonGroup != null)
        {
            //如果当前按钮被选中需要取消其他选中
            if (IsOn == true)
            {
                switch (buttonGroupType)
                {
                    case ButtonType.Normal:
                        buttonGroup.OneButtonChange(this);
                        break;
                    case ButtonType.State:
                        break;
                    case ButtonType.Rest:
                        buttonGroup.RestSelfButton();
                        break;
                }
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (interactable)
        {
            this.SendEvent(new SoundInfoEvent { soundName = buttonSoundName, soundType = SoundType.OnClick, soundVolume = 1f });
            IsOn = !isOn;
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (interactable)
        {
            IsPress();
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (interactable)
        {
            this.SendEvent(new SoundInfoEvent { soundName = buttonSoundName, soundType = SoundType.OnEnter, soundVolume = 1f });
            SetHover();
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (interactable)
        {
            ExitHover();
        }
    }


    /// <summary>
    /// 设置选中
    /// </summary>
    protected virtual void IsOnTrue()
    {
        WCButtonFactory.GetInstance().SetIsOnTrue(wcButtonType, this);
    }

    /// <summary>
    /// 设置未选中
    /// </summary>
    protected virtual void IsOnFalse()
    {
        WCButtonFactory.GetInstance().SetIsOnFalse(wcButtonType, this);
    }

    protected virtual void IsPress()
    {
        WCButtonFactory.GetInstance().SetIsPress(wcButtonType, this);
    }


    protected virtual void SetHover()
    {
        WCButtonFactory.GetInstance().SetHover(wcButtonType, this);
    }

    protected virtual void ExitHover()
    {
        WCButtonFactory.GetInstance().ExitHover(wcButtonType, this);
    }

    public IArchitecture GetArchitecture()
    {
        return GameCneter.Interface;
    }
}
