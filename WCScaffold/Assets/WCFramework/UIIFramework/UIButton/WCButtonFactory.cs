﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WCButtonFactory
{
    private static WCButtonFactory instance;

    public static WCButtonFactory GetInstance()
    {
        if (instance == null)
        {
            instance = new WCButtonFactory();
        }
        return instance;
    }

    public void SetIsOnFalse(WCButtonType wcButtonType, WCBaseButton wcBaseButton)
    {
        GetWCButtonBase(wcButtonType).IsOnFalse(wcBaseButton);
    }

    public void SetIsOnTrue(WCButtonType wcButtonType, WCBaseButton wcBaseButton)
    {
        GetWCButtonBase(wcButtonType).IsOnTrue(wcBaseButton);
    }

    public void SetIsPress(WCButtonType wcButtonType, WCBaseButton wcBaseButton)
    {
        GetWCButtonBase(wcButtonType).IsPress(wcBaseButton);
    }

    public void SetHover(WCButtonType wcButtonType, WCBaseButton wcBaseButton)
    {
        GetWCButtonBase(wcButtonType).SetHover(wcBaseButton);
    }

    public void ExitHover(WCButtonType wcButtonType, WCBaseButton wcBaseButton)
    {
        GetWCButtonBase(wcButtonType).ExitHover(wcBaseButton);
    }

    private WCButtonBase GetWCButtonBase(WCButtonType wcButtonType)
    {
        WCButtonBase wCButtonBase = null;
        switch (wcButtonType)
        {
            case WCButtonType.CommonImageButton:
                wCButtonBase = new WCCommonImageButton();
                break;
            case WCButtonType.CommonTextButton:
                wCButtonBase = new WCCommonCharacterButton();
                break;
            case WCButtonType.TextAndImageButton:
                wCButtonBase = new WCCommonCharAndImageButton();
                break;
            case WCButtonType.AllColorButton:
                wCButtonBase = new WCAllColorButton();
                break;
        }
        return wCButtonBase;
    }

}
