﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonGroup : MonoBehaviour
{
    private List<WCBaseButton> normalButtonList = new List<WCBaseButton>();

    /// <summary>
    /// 状态型按钮,可以单独选中
    /// 不参与按钮组的只能选中一个的规律
    /// 但参与重置按钮的取消选中
    /// </summary>
    private List<WCBaseButton> stateButtonList = new List<WCBaseButton>();

    public bool allowSwitchOff = false;

    /// <summary>
    /// 重置型按钮,点击后按钮组所有的按钮,全部取消选中
    /// </summary>
    public WCBaseButton RestSelfTypeButton { get; set; }

    /// <summary>
    /// 给按钮组添加普通按钮
    /// </summary>
    public void GroupAddNoramlButton(WCBaseButton oneButton)
    {
        normalButtonList.Add(oneButton);
    }

    /// <summary>
    /// 给按钮组添加状态按钮
    /// </summary>
    public void GroupAddStateButton(WCBaseButton oneButton)
    {
        stateButtonList.Add(oneButton);
    }

    /// <summary>
    /// 一个按钮被选中,如果只允许一个选中的话,就把其他按钮都设置成未选中
    /// </summary>
    public void OneButtonChange(WCBaseButton changeButton)
    {
        if (!allowSwitchOff)
        {
            foreach (WCBaseButton oneButton in normalButtonList)
            {
                if (oneButton != changeButton && oneButton != null)
                {
                    oneButton.IsOn = false;
                }
            }
        }
    }

    /// <summary>
    /// 重置按钮组
    /// </summary>
    public void RestSelfButton()
    {
        if (!allowSwitchOff)
        {
            foreach (WCBaseButton oneButton in normalButtonList)
            {
                oneButton.IsOn = false;
            }

            foreach (WCBaseButton oneButton in stateButtonList)
            {
                oneButton.IsOn = false;
            }

            RestSelfTypeButton.IsOn = false;
        }
    }

}
