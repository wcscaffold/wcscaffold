﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WCCommonImageButton : WCButtonBase
{
    public override void IsOnFalse(WCBaseButton wcBaseButton)
    {
        base.IsOnFalse(wcBaseButton);

        Transition(wcBaseButton.buttonImage, wcBaseButton.normalTexture, wcBaseButton.normalColor);

        HideOrShowTips(wcBaseButton, false);
    }

    public override void IsOnTrue(WCBaseButton wcBaseButton)
    {
        base.IsOnTrue(wcBaseButton);
        if (wcBaseButton.isCanSelect)
        {
            Transition(wcBaseButton.buttonImage, wcBaseButton.selectTexture, wcBaseButton.selectColor);
        }
        else
        {
            Transition(wcBaseButton.buttonImage, wcBaseButton.normalTexture, wcBaseButton.normalColor);
        }
        HideOrShowTips(wcBaseButton, false);
    }

    public override void IsPress(WCBaseButton wcBaseButton)
    {
        base.IsPress(wcBaseButton);
        Transition(wcBaseButton.buttonImage, wcBaseButton.pressTexture, wcBaseButton.pressColor);
        HideOrShowTips(wcBaseButton, false);
    }

    public override void SetHover(WCBaseButton wcBaseButton)
    {
        base.SetHover(wcBaseButton);
        HideOrShowTips(wcBaseButton, true);
        Transition(wcBaseButton.buttonImage, wcBaseButton.hoverTexture, wcBaseButton.hoverColor);
    }

    public override void ExitHover(WCBaseButton wcBaseButton)
    {
        base.ExitHover(wcBaseButton);
        if (wcBaseButton.IsOn)
        {
            IsOnTrue(wcBaseButton);
        }
        else
        {
            IsOnFalse(wcBaseButton);
        }
        HideOrShowTips(wcBaseButton, false);
    }

    /// <summary>
    /// 隐藏或显示提示
    /// </summary>
    private void HideOrShowTips(WCBaseButton wcBaseButton, bool isShow)
    {
        if (wcBaseButton.tipsObj != null)
        {
            wcBaseButton.tipsObj.SetActive(isShow);
        }
    }

}
