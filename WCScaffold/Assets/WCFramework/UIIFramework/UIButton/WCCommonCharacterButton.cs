﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// 纯文字按钮有问题，可能需要多种叠加颜色
/// </summary>
public class WCCommonCharacterButton : WCButtonBase
{
    public override void IsOnFalse(WCBaseButton wcBaseButton)
    {
        base.IsOnFalse(wcBaseButton);

        if (wcBaseButton.buttonText.color != wcBaseButton.hoverColor)
        {
            TextTransition(wcBaseButton.buttonText, wcBaseButton.normalColor);
        }
        else
        {
            wcBaseButton.buttonText.color = wcBaseButton.normalColor;
        }
        if (wcBaseButton.selectImageObj != null)
        {
            wcBaseButton.selectImageObj.SetActive(false);
        }
    }

    public override void IsOnTrue(WCBaseButton wcBaseButton)
    {
        base.IsOnTrue(wcBaseButton);
        if (wcBaseButton.buttonText.color != wcBaseButton.hoverColor)
        {
            if (wcBaseButton.isCanSelect)
            {
                TextTransition(wcBaseButton.buttonText, wcBaseButton.selectColor);

            }
            else
            {
                TextTransition(wcBaseButton.buttonText, wcBaseButton.normalColor);
            }
        }
        else
        {
            if (wcBaseButton.isCanSelect)
            {
                wcBaseButton.buttonText.color = wcBaseButton.selectColor;
            }
            else
            {
                wcBaseButton.buttonText.color = wcBaseButton.normalColor;
            }
        }
        if (wcBaseButton.selectImageObj != null)
        {
            wcBaseButton.selectImageObj.SetActive(true);
        }
    }

    public override void IsPress(WCBaseButton wcBaseButton)
    {
        base.IsPress(wcBaseButton);
        wcBaseButton.buttonText.color = wcBaseButton.pressColor;

    }

    public override void SetHover(WCBaseButton wcBaseButton)
    {
        base.SetHover(wcBaseButton);
        wcBaseButton.buttonText.color = wcBaseButton.hoverColor;
    }

    public override void ExitHover(WCBaseButton wcBaseButton)
    {
        base.ExitHover(wcBaseButton);
        if (wcBaseButton.IsOn)
        {
            IsOnTrue(wcBaseButton);
        }
        else
        {
            IsOnFalse(wcBaseButton);
        }
    }
}
