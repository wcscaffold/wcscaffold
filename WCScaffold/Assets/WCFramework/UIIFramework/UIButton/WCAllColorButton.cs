﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WCAllColorButton : WCButtonBase
{
    public override void IsOnFalse(WCBaseButton wcBaseButton)
    {
        base.IsOnFalse(wcBaseButton);

        if (wcBaseButton.buttonText.color != wcBaseButton.hoverColor)
        {
            TextTransition(wcBaseButton.buttonText, wcBaseButton.normalColor);
        }
        else
        {
            wcBaseButton.buttonText.color = wcBaseButton.normalColor;
        }
        Transition(wcBaseButton.buttonImage, null, wcBaseButton.imageNormalColor);
        HideOrShowTips(wcBaseButton, false);

    }

    public override void IsOnTrue(WCBaseButton wcBaseButton)
    {
        base.IsOnTrue(wcBaseButton);

        if (wcBaseButton.buttonText.color != wcBaseButton.hoverColor)
        {
            if (wcBaseButton.isCanSelect)
            {
                TextTransition(wcBaseButton.buttonText, wcBaseButton.selectColor);

            }
            else
            {
                TextTransition(wcBaseButton.buttonText, wcBaseButton.normalColor);
            }
        }
        else
        {
            if (wcBaseButton.isCanSelect)
            {
                wcBaseButton.buttonText.color = wcBaseButton.selectColor;
            }
            else
            {
                wcBaseButton.buttonText.color = wcBaseButton.normalColor;
            }
        }

        if (wcBaseButton.isCanSelect)
        {
            Transition(wcBaseButton.buttonImage, null, wcBaseButton.imageSelectColor);
        }
        else
        {
            Transition(wcBaseButton.buttonImage, null, wcBaseButton.imageNormalColor);
        }

        HideOrShowTips(wcBaseButton, false);
    }

    public override void IsPress(WCBaseButton wcBaseButton)
    {
        base.IsPress(wcBaseButton);
        wcBaseButton.buttonText.color = wcBaseButton.pressColor;
        Transition(wcBaseButton.buttonImage, null, wcBaseButton.imagePressColor);
        HideOrShowTips(wcBaseButton, false);

    }

    public override void SetHover(WCBaseButton wcBaseButton)
    {
        base.SetHover(wcBaseButton);
        HideOrShowTips(wcBaseButton, true);
        Transition(wcBaseButton.buttonImage, null, wcBaseButton.imageHoverColor);
        wcBaseButton.buttonText.color = wcBaseButton.hoverColor;

    }

    public override void ExitHover(WCBaseButton wcBaseButton)
    {
        base.ExitHover(wcBaseButton);
        if (wcBaseButton.IsOn)
        {
            IsOnTrue(wcBaseButton);
        }
        else
        {
            IsOnFalse(wcBaseButton);
        }
    }


    /// <summary>
    /// 隐藏或显示提示
    /// </summary>
    private void HideOrShowTips(WCBaseButton wcBaseButton, bool isShow)
    {
        if (wcBaseButton.tipsObj != null)
        {
            wcBaseButton.tipsObj.SetActive(isShow);
        }
    }
}
