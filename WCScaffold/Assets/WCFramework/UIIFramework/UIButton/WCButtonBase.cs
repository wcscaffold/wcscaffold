﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WCButtonBase
{
    private Color colorless = new Color(1, 1, 1, 1);

    /// <summary>
    /// 设置选中
    /// </summary>
    public virtual void IsOnTrue(WCBaseButton wcBaseButton)
    {

    }

    /// <summary>
    /// 设置未选中
    /// </summary>
    public virtual void IsOnFalse(WCBaseButton wcBaseButton)
    {

    }

    public virtual void IsPress(WCBaseButton wcBaseButton)
    {

    }


    public virtual void SetHover(WCBaseButton wcBaseButton)
    {

    }

    public virtual void ExitHover(WCBaseButton wcBaseButton)
    {

    }

    /// <summary>
    /// 按钮的渐隐渐显动画
    /// </summary>
    protected virtual void Transition(Image needChangeButton, Sprite needChangeSprite, Color color)
    {
        if (needChangeSprite == null)
        {
            needChangeButton.color = color;
        }
        else
        {
            needChangeButton.color = colorless;
            if (needChangeButton.sprite != needChangeSprite)
            {
                Tween tweener = ButtonFade(needChangeButton);
                tweener.Complete(ButtonDisplay(needChangeButton, needChangeSprite));
            }
        }
    }

    /// <summary>
    /// 按钮渐隐
    /// </summary>
    private Tween ButtonFade(Image needChangeButton)
    {
        return DOTween.ToAlpha(() => needChangeButton.color, p => needChangeButton.color = p, 0, 0.2f);
    }

    /// <summary>
    /// 按钮渐显
    /// </summary>
    private bool ButtonDisplay(Image needChangeButton, Sprite needChangeSprite)
    {
        needChangeButton.sprite = needChangeSprite;
        needChangeButton.color = new Color(needChangeButton.color.r, needChangeButton.color.g, needChangeButton.color.b, 0);
        Tweener tweener = DOTween.ToAlpha(() => needChangeButton.color, p => needChangeButton.color = p, 1, 0.2f);

        return true;
    }

    protected virtual void TextTransition(Text text, Color color)
    {
        if (text.color != color)
        {
            Tween tweener = TextButtonFade(text);
            tweener.Complete(TextButtonDisplay(text, color));
        }
    }

    private Tween TextButtonFade(Text text)
    {
        return DOTween.ToAlpha(() => text.color, p => text.color = p, 0, 0.2f);
    }

    /// <summary>
    /// 文字按钮渐显
    /// </summary>
    private bool TextButtonDisplay(Text text, Color color)
    {
        text.color = color;
        Tweener tweener = DOTween.ToAlpha(() => text.color, p => text.color = p, 1, 0.2f);

        return true;
    }
}
