﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WCCommonCharAndImageButton : WCCommonImageButton
{
    public override void IsOnFalse(WCBaseButton wcBaseButton)
    {
        base.IsOnFalse(wcBaseButton);
        if (wcBaseButton.buttonText.color != wcBaseButton.hoverColor)
        {
            TextTransition(wcBaseButton.buttonText, wcBaseButton.normalColor);
        }
        else
        {
            wcBaseButton.buttonText.color = wcBaseButton.normalColor;
        }
    }

    public override void IsOnTrue(WCBaseButton wcBaseButton)
    {
        base.IsOnTrue(wcBaseButton);
        if (wcBaseButton.buttonText.color != wcBaseButton.hoverColor)
        {
            if (wcBaseButton.isCanSelect)
            {
                TextTransition(wcBaseButton.buttonText, wcBaseButton.selectColor);

            }
            else
            {
                TextTransition(wcBaseButton.buttonText, wcBaseButton.normalColor);
            }
        }
        else
        {
            if (wcBaseButton.isCanSelect)
            {
                wcBaseButton.buttonText.color = wcBaseButton.selectColor;
            }
            else
            {
                wcBaseButton.buttonText.color = wcBaseButton.normalColor;
            }
        }
    }

    public override void IsPress(WCBaseButton wcBaseButton)
    {
        base.IsPress(wcBaseButton);
        wcBaseButton.buttonText.color = wcBaseButton.pressColor;

    }

    public override void SetHover(WCBaseButton wcBaseButton)
    {
        base.SetHover(wcBaseButton);
        //wcBaseButton.buttonText.color = wcBaseButton.hoverColor;
    }

    public override void ExitHover(WCBaseButton wcBaseButton)
    {
        base.ExitHover(wcBaseButton);
        if (wcBaseButton.IsOn)
        {
            IsOnTrue(wcBaseButton);
        }
        else
        {
            IsOnFalse(wcBaseButton);
        }
    }
}
