﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;

public interface ITaskManagerUtility : IUtility
{
    /// <summary>
    /// 添加时间任务
    /// </summary>
    /// <returns>返回任务id</returns>
    int AddTask(TaskDelegate taskEvent, int runCount, bool isLoop, TimeType timeType, float delayTime, float intervalTime = 0);

    /// <summary>
    /// 删除任务
    /// </summary>
    /// <param name="id">要删除的任务id</param>
    void DeleteTask(int id);

}
public class TaskManagerUtility : MonoBehaviour, ITaskManagerUtility
{
    public List<TaskData> tasks = new List<TaskData>();

    public int initID = 0;


    // Update is called once per frame
    void Update()
    {
        CheckTasks();
    }

    public int AddTask(TaskDelegate taskEvent, int runCount, bool isLoop, TimeType timeType, float delayTime, float intervalTime = 0)
    {
        int id = GetID();
        TaskData task = new TaskData(TaskType.TIME, taskEvent, id, runCount, isLoop, timeType, GetRealTime(timeType, delayTime), GetRealTime(timeType, intervalTime));
        tasks.Add(task);
        Debug.Log(string.Format("任务已添加  ID:{0} ", id));
        if (task == null)
        {
            Debug.Log(string.Format("ID:{0}任务添加失败 ", id));
            return -1;
        }
        else
        {
            return id;
        }
    }

    //添加帧任务
    public int AddTask_Frame(TaskDelegate taskEvent, int runCount, bool isLoop, int delayFrame, int intervalFrame = 0)
    {
        int id = GetID();
        TaskData task = new TaskData(TaskType.FRAME, taskEvent, id, runCount, isLoop, delayFrame, intervalFrame);
        tasks.Add(task);
        Debug.Log(string.Format("帧任务已添加  ID:{0} ", id));
        if (task == null)
        {
            Debug.Log(string.Format("ID:{0}帧任务添加失败 ", id));
            return -1;
        }
        else
        {
            return id;
        }

    }

    public void DeleteTask(int id)
    {
        for (int i = 0; i < tasks.Count; i++)
        {
            if (tasks[i].Id == id)
            {
                tasks.RemoveAt(i);
                Debug.Log(string.Format("ID:{0}  任务已删除", id));
                return;
            }
        }
        Debug.LogError(string.Format("ID:{0}  任务不存在，删除失败", id));
    }


    /// <summary>
    /// 替换时间任务
    /// </summary>
    /// <param name="id">要替换的任务id</param>
    public void ReplaceTask(int id, TaskDelegate taskEvent, int runCount, bool isLoop, TimeType timeType, float delayTime, float intervalTime = 0)
    {
        for (int i = 0; i < tasks.Count; i++)
        {
            if (tasks[i].Id == id)
            {
                tasks[i].TaskEvent = taskEvent;
                tasks[i].RunCount = runCount;
                tasks[i].IsLoop = isLoop;
                tasks[i].TimeType = timeType;
                tasks[i].DelayTime = GetRealTime(timeType, delayTime);
                tasks[i].RealRunTime = Time.realtimeSinceStartup * 1000 + delayTime;
                tasks[i].IntervalTime = GetRealTime(timeType, intervalTime);
                Debug.Log(string.Format("ID:{0}  任务替换成功", id));
                return;
            }
        }
        Debug.LogError(string.Format("ID:{0}  任务不存在，替换失败", id));
    }


    /// <summary>
    /// 替换帧任务
    /// </summary>
    /// <param name="id"></param>
    public void ReplaceTask_Frame(int id, TaskDelegate taskEvent, int runCount, bool isLoop, int delayFrame, int intervalFrame = 0)
    {
        for (int i = 0; i < tasks.Count; i++)
        {
            if (tasks[i].Id == id)
            {
                tasks[i].TaskEvent = taskEvent;
                tasks[i].RunCount = runCount;
                tasks[i].IsLoop = isLoop;
                tasks[i].DelayFrame = delayFrame;
                tasks[i].RealRunFrame = Time.frameCount + delayFrame;
                tasks[i].IntervalFrame = intervalFrame;
                Debug.Log(string.Format("ID:{0}  帧任务替换成功", id));
                return;
            }
        }
        Debug.LogError(string.Format("ID:{0}  帧任务不存在，替换失败", id));
    }


    public int GetID()
    {
        initID++;
        return initID;
    }


    /// <summary>
    /// 将时间类型转换为毫秒
    /// </summary>
    /// <param name="timeType">时间类型</param>
    /// <param name="delayTime">时间度量</param>
    /// <returns></returns>
    public float GetRealTime(TimeType timeType, float delayTime)
    {
        float realtime = 0;
        switch (timeType)
        {
            case TimeType.MILLISECOND:
                realtime = delayTime;
                break;
            case TimeType.SECOND:
                realtime = delayTime * 1000;
                break;
            case TimeType.MINUTE:
                realtime = delayTime * 1000 * 60;
                break;
            case TimeType.HOUR:
                realtime = delayTime * 1000 * 60 * 60;
                break;
            case TimeType.DAY:
                realtime = delayTime * 1000 * 60 * 60 * 60;
                break;
            case TimeType.YEAR:
                realtime = delayTime * 1000 * 60 * 60 * 60 * 365;
                break;
            default:
                break;
        }
        return realtime;
    }

    /// <summary>
    /// 检查任务是否应该执行
    /// </summary>
    private void CheckTasks()
    {
        for (int i = 0; i < tasks.Count; i++)
        {
            TaskData task = tasks[i];
            if (task.TaskType == TaskType.TIME)
            {
                if (Time.realtimeSinceStartup * 1000 > tasks[i].RealRunTime)
                {
                    task.TaskEvent?.Invoke();


                    if (task.IsLoop == true)
                    {
                        task.RealRunTime += task.IntervalTime;
                    }
                    else
                    {
                        task.RunCount--;
                        if (task.RunCount > 0)
                        {
                            task.RealRunTime += task.IntervalTime;
                        }
                        else
                        {
                            Debug.Log(string.Format("ID:{0}  任务次数为零，已删除任务", tasks[i].Id));
                            tasks.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }

            if (task.TaskType == TaskType.FRAME)
            {
                if (Time.frameCount > tasks[i].RealRunFrame)
                {
                    task.TaskEvent?.Invoke();


                    if (task.IsLoop == true)
                    {
                        task.RealRunFrame += task.IntervalFrame;
                    }
                    else
                    {
                        task.RunCount--;
                        if (task.RunCount > 0)
                        {
                            task.RealRunFrame += task.IntervalFrame;
                        }
                        else
                        {
                            Debug.Log(string.Format("ID:{0}  帧任务次数为零，已删除任务", tasks[i].Id));
                            tasks.RemoveAt(i);
                            i--;
                        }
                    }
                }
            }
        }

    }
}

//任务事件的委托
public delegate void TaskDelegate();

//时间类型
public enum TimeType
{
    MILLISECOND,
    SECOND,
    MINUTE,
    HOUR,
    DAY,
    YEAR,
}

//任务类型
public enum TaskType
{
    FRAME,  //帧任务
    TIME,   //时间任务
}


public class TaskData
{
    private TaskDelegate taskEvent;//需要执行的具体任务
    private int id;               //任务id
    private int runCount;         //执行次数
    private bool isLoop;          //是否循环
    private TimeType timeType;    //时间类型
    private float delayTime;      //延迟时间
    private float realRunTime;    //真正执行的时间，和startuptime比较
    private float intervalTime;   //循环任务的调用时间间隔,默认为0

    //帧任务需要的成员变量
    private TaskType taskType;     //任务类型
    private int delayFrame;       //相隔多少帧调用一次
    private int realRunFrame;    //真正执行的帧数，和frameCount 比较
    private int intervalFrame;   //循环任务的调用帧数间隔,默认为0


    //封装一哈
    public TaskDelegate TaskEvent { get => taskEvent; set => taskEvent = value; }
    public int Id { get => id; set => id = value; }
    public int RunCount { get => runCount; set => runCount = value; }
    public bool IsLoop { get => isLoop; set => isLoop = value; }
    public TimeType TimeType { get => timeType; set => timeType = value; }
    public float DelayTime { get => delayTime; set => delayTime = value; }
    public float RealRunTime { get => realRunTime; set => realRunTime = value; }
    public float IntervalTime { get => intervalTime; set => intervalTime = value; }
    public TaskType TaskType { get => taskType; set => taskType = value; }
    public int DelayFrame { get => delayFrame; set => delayFrame = value; }
    public int RealRunFrame { get => realRunFrame; set => realRunFrame = value; }
    public int IntervalFrame { get => intervalFrame; set => intervalFrame = value; }


    public TaskData()
    {

    }

    //时间类型任务构造函数
    public TaskData(TaskType taskType, TaskDelegate taskEvent, int id, int runCount, bool isLoop, TimeType timeType, float delayTime, float intervalTime = 0)
    {
        TaskType = taskType;
        TaskEvent = taskEvent;
        Id = id;
        RunCount = runCount;
        IsLoop = isLoop;
        TimeType = timeType;
        DelayTime = delayTime;
        RealRunTime = Time.realtimeSinceStartup * 1000 + delayTime;
        IntervalTime = intervalTime;
    }


    //帧类型任务构造函数
    public TaskData(TaskType taskType, TaskDelegate taskEvent, int id, int runCount, bool isLoop, int delayFrame, int intervalFrame = 0)
    {
        TaskType = taskType;
        TaskEvent = taskEvent;
        Id = id;
        RunCount = runCount;
        IsLoop = isLoop;
        DelayFrame = delayFrame;
        RealRunFrame = Time.frameCount + delayFrame;
        IntervalFrame = intervalFrame;
    }
}

