﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;
using System;

public interface IObjectOperation : IUtility
{
    /// <summary>
    /// 加载资源,并根据isCatch是不是将资源加入缓存
    /// </summary>
    /// <param name="path"> 资源地址 </param>
    /// <param name="isCatch"> 是否将资源放入缓存 </param>
    /// <returns></returns>
    T LoadResource<T>(string path, bool isCatch) where T : UnityEngine.Object;
    /// <summary>
    /// 实例化资源
    /// </summary>
    /// <param name="path">  资源路径 </param>
    /// <param name="isCatch"> 是否将资源放入缓存 </param>
    /// <returns></returns>
    GameObject InsAsset(string path, bool isCatch = true);

    /// <summary>
    /// 异步加载实例化资源
    /// </summary>
    /// <param name="path"></param>
    /// <param name="loadOverDo"></param>
    void InsAssetAsync(string path, Action<GameObject> loadOverDo);
}

public class ObjectOperation : MonoBehaviour, IObjectOperation
{
    private Dictionary<string, UnityEngine.Object> exitAsset = new Dictionary<string, UnityEngine.Object>();

    public T LoadResource<T>(string path, bool isCatch) where T : UnityEngine.Object
    {
        if (exitAsset.ContainsKey(path))
        {
            return exitAsset[path] as T;
        }

        T objResource = Resources.Load<T>(path) as T;
        if (objResource == null)
        {
            Debug.LogError(GetType() + "LoadResource 提取资源找不到 路径 path=" + path);
        }
        else if (isCatch)
        {
            exitAsset.Add(path, objResource);
        }

        return objResource;
    }

    /// <summary>
    /// 实例化资源
    /// </summary>
    /// <param name="path">资源路径</param>
    /// <param name="isCatch">是否将资源放入缓存</param>
    /// <returns></returns>
    public GameObject InsAsset(string path, bool isCatch = true)
    {
        GameObject objResource = LoadResource<GameObject>(path, isCatch);
        GameObject objClone = GameObject.Instantiate(objResource);
        if (objClone == null)
        {
            Debug.Log(GetType() + "InsAsset 实例资源失败,请检查 路径 path=" + path);
        }
        return objClone;
    }

    public void InsAssetAsync(string path, Action<GameObject> loadOverDo)
    {
        StartCoroutine(LoadResAsync(path, loadOverDo));
    }

    private IEnumerator LoadResAsync(string path, Action<GameObject> loadOverDo)
    {
        Debug.Log("wcwcs");
        //异步加载
        ResourceRequest r = Resources.LoadAsync<GameObject>(path);
        yield return r;
        if (r.asset == null)
        {
            Debug.Log(GetType() + "InsAsset 实例资源失败,请检查 路径 path=" + path);
        }
        //没有记录过的资源，记录之
        if (!exitAsset.ContainsKey(path))
        {
            exitAsset.Add(path, r.asset as GameObject);
        }
        GameObject newObj = GameObject.Instantiate(exitAsset[path] as GameObject);

        if (loadOverDo != null)
        {
            loadOverDo(newObj);
        }
    }
}
