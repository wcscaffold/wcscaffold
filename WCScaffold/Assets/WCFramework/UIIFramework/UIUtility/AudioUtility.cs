﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;

public interface IAudioUtility : IUtility
{
    /// <summary>
    /// 背景音频
    /// </summary>
    AudioClip Clip { set; get; }

    /// <summary>
    /// 播放后的事件
    /// </summary>
    Action<float> AfterPlaying { get; set; }

    /// <summary>
    /// 播放状态
    /// </summary>
    AudioState AudioState { get; }

    /// <summary>
    /// 音量
    /// </summary>
    float Volume { get; set; }

    /// <summary>
    /// 播放音效
    /// </summary>
    void PlayOneShot(AudioClip clip, float volumeScale);

    /// <summary>
    /// 开始播放背景音乐
    /// </summary>
    void Play();
}


public class AudioUtility : MonoBehaviour, IAudioUtility
{
    private float playTime = 0;
    private AudioSource AudioSource;
    public AudioState AudioState { get; private set; }

    public Action<float> AfterPlaying { get; set; }

    private void Awake()
    {
        this.AudioSource = this.gameObject.AddComponent<AudioSource>();

    }

    public AudioClip Clip
    {
        get
        {
            return this.AudioSource.clip;
        }
        set
        {
            this.AudioSource.clip = value;
            playTime = 0;
        }
    }
    public bool Loop
    {
        get
        {
            return this.AudioSource.loop;
        }
        set
        {
            this.AudioSource.loop = value;
        }
    }
    public float Volume
    {
        get
        {
            return this.AudioSource.volume;
        }
        set
        {
            this.AudioSource.volume = value;
        }
    }

    public void Play()
    {
        if (null == this.AudioSource)
        {
            return;
        }
        this.AudioState = AudioState.IsPlaying;
        this.AudioSource.Play();
    }
    public void Pause()
    {
        if (null == this.AudioSource)
        {
            return;
        }
        if (this.AudioSource.isPlaying)
        {
            this.AudioState = AudioState.Pause;
            this.AudioSource.Pause();
        }
    }
    public void Stop()
    {
        if (null == this.AudioSource)
        {
            return;
        }
        this.AudioState = AudioState.Stop;
        this.AudioSource.Stop();
        if (AfterPlaying != null)
        {
            this.AfterPlaying(Volume);
        }
    }

    public void PlayOneShot(AudioClip clip, float volumeScale)
    {
        this.AudioSource.PlayOneShot(clip, volumeScale);
    }

    private void Update()
    {
        if (this.AudioSource != null && this.AudioSource.clip != null && this.AudioState == AudioState.IsPlaying)
        {
            playTime += Time.deltaTime;
            if (playTime >= this.Clip.length)
            {
                Debug.Log("this.Clip.length : " + this.Clip.length);
                Debug.Log("playTime : " + playTime);

                playTime = 0;
                this.Stop();
            }
        }
    }
}

public enum AudioState
{
    Idle,
    IsPlaying,
    Pause,
    Stop,
}

