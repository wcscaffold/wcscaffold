﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;

public class ChangeSceneCommand : AbstractCommand
{
    private SceneName sceneName;

    public ChangeSceneCommand(SceneName sceneName)
    {
        this.sceneName = sceneName;
    }

    protected override void OnExecute()
    {
        this.GetModel<IGameModel>().SceneName.Value = sceneName.ToString();
    }
}
