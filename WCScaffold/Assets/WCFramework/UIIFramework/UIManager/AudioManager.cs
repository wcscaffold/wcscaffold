﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;

public struct SoundInfoEvent
{
    /// <summary>
    /// 声音的类型
    /// </summary>
    public SoundType soundType;

    /// <summary>
    /// 声音的名称，用来找声音
    /// </summary>
    public SoundName soundName;

    /// <summary>
    /// 声音的初始音量
    /// </summary>
    public float soundVolume;
}

public struct ChangeSoundVolumeEvent
{
    /// <summary>
    /// 声音的类型
    /// </summary>
    public SoundType soundType;

}

public enum SoundName
{
    None,
    /// <summary>
    /// 按钮通用音效
    /// </summary>
    ButtonGeneral,
    /// <summary>
    /// 登录按钮音效
    /// </summary>
    LoginButtonGeneral,
    /// <summary>
    /// 背景音效
    /// </summary>
    BG
}

public enum SoundType
{
    None,
    /// <summary>
    /// 按钮点击音效
    /// </summary>
    OnClick,
    /// <summary>
    /// 按钮进入音效
    /// </summary>
    OnEnter,
    /// <summary>
    /// 背景音效
    /// </summary>
    OnBG
}

public class AudioManager : MonoBehaviour, IController
{
    private const string PathPrefix = "Sounds/";
    private const string PathBGPrefix = "Sounds/BGSounds/";

    private const int bgSoundVolume = 1;
    private void Awake()
    {
        this.RegisterEvent<SoundInfoEvent>(PlayNormalSound);
        this.RegisterEvent<ChangeSoundVolumeEvent>(ChangeVolume);
    }

    public void PlayNormalSound(SoundInfoEvent soundInfoEvent)
    {
        switch (soundInfoEvent.soundType)
        {
            case SoundType.OnEnter:
            case SoundType.OnClick:
                PlayOneShootSound(LoadSound(PathPrefix, soundInfoEvent.soundName.ToString()),
                    soundInfoEvent.soundVolume * GameSetUpForm.mainVolumeCount * GameSetUpForm.clickVolumeCount);
                break;
            case SoundType.OnBG:
                PlayBGSound(soundInfoEvent.soundVolume * GameSetUpForm.mainVolumeCount * GameSetUpForm.bgVolumeCount);
                break;
        }
    }

    /// <summary>
    /// 更改音量
    /// </summary>
    private void ChangeVolume(ChangeSoundVolumeEvent changeSoundVolumeEvent)
    {
        IAudioUtility audioUtility = this.GetUtility<IAudioUtility>();

        switch (changeSoundVolumeEvent.soundType)
        {
            case SoundType.None:
                audioUtility.Volume = bgSoundVolume * GameSetUpForm.mainVolumeCount * GameSetUpForm.bgVolumeCount;
                break;
            case SoundType.OnBG:
                audioUtility.Volume = bgSoundVolume * GameSetUpForm.mainVolumeCount * GameSetUpForm.bgVolumeCount;
                break;
        }
    }

    /// <summary>
    /// 播放音效
    /// </summary>
    private void PlayOneShootSound(AudioClip audioClip, float volume)
    {
        IAudioUtility audioUtility = this.GetUtility<IAudioUtility>();
        audioUtility.PlayOneShot(audioClip, volume);
    }

    /// <summary>
    /// 播放背景音乐
    /// </summary>
    private void PlayBGSound(float volume)
    {
        int bgSoundIndex = Random.Range(1, 8);
        IAudioUtility audioUtility = this.GetUtility<IAudioUtility>();
        audioUtility.Clip = LoadSound(PathBGPrefix, bgSoundIndex.ToString());
        audioUtility.Volume = volume;
        audioUtility.Play();
        audioUtility.AfterPlaying = PlayBGSound;
    }

    private AudioClip LoadSound(string path, string soundName)
    {
        Debug.Log(path + soundName);
        AudioClip audioClip = (AudioClip)Resources.Load(path + soundName);
        return audioClip;
    }

    public IArchitecture GetArchitecture()
    {
        return GameCneter.Interface;
    }
}
