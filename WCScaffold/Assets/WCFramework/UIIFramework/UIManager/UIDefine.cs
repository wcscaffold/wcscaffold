﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDefine : MonoBehaviour
{
    public const string SYS_PATH_CANVAS = "UIPrefab/Canvas";
    public const string SYS_TAG_CANVAS = "_TagCanvas";
    public const string SYS_TAG_UICAMERA = "_TagUICamera";

    public const string SYS_FORM_NORMAL = "Normal";
    public const string SYS_FORM_FIXED = "Fixed";
    public const string SYS_FORM_POPUP = "PopUp";
    public const string SYS_PANEL_MASK = "_UIMaskPanel";

    /* 遮罩管理器中,透明度常量 */
    public const float SYS_UIMASK_LUCENCY_COLOR_RGB = 255 / 255F;
    public const float SYS_UIMASK_LUCENCY_COLOR_RGB_A = 0F / 255F;

    public const float SYS_UIMASK_TRANS_LUCENCY_COLOR_RGB = 0 / 255F;
    public const float SYS_UIMASK_TRANS_LUCENCY_COLOR_RGB_A = 200 / 255F;

    public const float SYS_UIMASK_IMPENETRABLE_COLOR_RGB = 0 / 255F;
    public const float SYS_UIMASK_IMPENETRABLE_COLOR_RGB_A = 60 / 255F;

    public static bool isOpenValveLabel = false;

    public static string roomCode = "-1";
}
//UI窗体（位置）类型
public enum UIFormType
{
    //普通窗体
    Normal,
    //固定窗体                              
    Fixed,
    //弹出窗体
    PopUp
}

//UI窗体的显示类型
public enum UIFormShowMode
{
    //普通
    Normal,
    //反向切换
    ReverseChange,
    //隐藏其他
    HideOther
}

//UI窗体透明度类型
public enum UIFormLucencyType
{
    /// <summary>
    /// 完全透明,不能穿透
    /// </summary>
    Lucency,
    /// <summary>
    /// 半透明,不能穿透
    /// </summary>
    Translucence,
    /// <summary>
    /// 低透明度,不能穿透
    /// </summary>
    ImPenetrable,
    /// <summary>
    /// 可以穿透
    /// </summary>
    Pentrate
}