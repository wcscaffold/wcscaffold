﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualSimulationMessageCenter : MonoBehaviour
{
    public delegate void MessageEventHandler(MessageDetial uIMessageDetial);
    //string 消息类型。MessageEventHandler 委托
    public static Dictionary<VirtualMessageType, MessageEventHandler> messageDic = new Dictionary<VirtualMessageType, MessageEventHandler>();

    /// <summary>
    /// 增加消息监听
    /// </summary>
    public static void AddMsgListener(VirtualMessageType messageType, MessageEventHandler messageEventHandler)
    {
        if (!messageDic.ContainsKey(messageType))
        {
            messageDic.Add(messageType, null);
        }
        messageDic[messageType] += messageEventHandler;
    }

    /// <summary>
    /// 取消消息监听一个类型中的一个
    /// </summary>
    public static void RemoveMsgListener(VirtualMessageType messageType, MessageEventHandler messageEventHandler)
    {
        if (messageDic.ContainsKey(messageType))
        {
            messageDic[messageType] -= messageEventHandler;
        }
    }

    /// <summary>
    /// 取消消息监听一个类型的所有
    /// </summary>
    public static void RemoveMsgListener(VirtualMessageType messageType)
    {
        if (messageDic.ContainsKey(messageType))
        {
            messageDic.Remove(messageType);
        }
    }

    /// <summary>
    /// 删除所有监听
    /// </summary>
    public static void ClearMsgListener()
    {
        if (messageDic != null)
        {
            messageDic.Clear();
        }
    }

    /// <summary>
    /// 执行监听事件 
    /// </summary>
    /// <param name="messageType"> 数据类型</param>
    /// <param name="uIMessageDetial"> 需要发送的数据 </param>
    public static void ImplementMsgListener(VirtualMessageType messageType, MessageDetial uIMessageDetial)
    {
        MessageEventHandler messageEventHandler;
        if (messageDic.TryGetValue(messageType, out messageEventHandler))
        {
            if (messageEventHandler != null)
            {
                messageEventHandler(uIMessageDetial);
            }
        }
    }


}

public class MessageDetial
{
    private object dataDetial;

    private Action<object> callBackAction;


    public object DataDetial
    {
        get { return dataDetial; }
    }

    public Action<object> CallBackAction
    {
        get { return callBackAction; }
    }

    public MessageDetial(object detaDetial)
    {
        this.dataDetial = detaDetial;
    }



    public MessageDetial(Action<object> callBackAction)
    {
        this.callBackAction = callBackAction;
    }

}

/// <summary>
/// 消息类型
/// </summary>
public enum VirtualMessageType
{
    /// <summary>
    /// 播放普通的声音
    /// </summary>
    PlayNormalSound,
    /// <summary>
    /// 创建一个ui的p层
    /// </summary>
    CreateOneUIPresent,
    /// <summary>
    /// 返回上一个页面
    /// </summary>
    ReturnLastView,
    /// <summary>
    /// 更新器械的数据
    /// </summary>
    UpdateDeviceData,
    /// <summary>
    /// 隐藏所有近景阀门
    /// </summary>
    HideAllCloseShotValve,
    /// <summary>
    /// 设置展示的近景阀门类型
    /// </summary>
    SetCloseShotValveType,
    /// <summary>
    /// 设置近景阀门的角度通过阀门旋转多少角度
    /// </summary>
    SetCloseShotValveAnglesByRoate,
    /// <summary>
    /// 设置近景阀门的角度通过阀门当前角度
    /// </summary>
    SetCloseShotValveAnglesByAngle,
    /// <summary>
    /// 设置近景开度值显示
    /// </summary>
    SetApertureText,
    /// <summary>
    /// 设置设备信息
    /// </summary>
    SetEquipmentInfo,
    /// <summary>
    /// 设置器材的位置和名称
    /// </summary>
    SetEquipmentLocationAndName,
    /// <summary>
    /// 设置器材m某个部件的位置和名称
    /// </summary>
    SetComponentLocationAndName,
    /// <summary>
    /// 打开器材介绍
    /// </summary>
    OpenEquipmentIntroduction,
    /// <summary>
    /// 设置控制器材的类型
    /// </summary>
    SetControllerEquipmentType,
    /// <summary>
    /// 设置当前拆分的模型并加载他
    /// </summary>
    SetAndLoadEquipment,
    /// <summary>
    /// 初始化部件的位置和高亮状态
    /// </summary>
    InitComponent,
    /// <summary>
    /// 加载并展示零件图标
    /// </summary>
    LoadComponentSprite,
    /// <summary>
    /// 打开和关闭管道特效
    /// </summary>
    OpenOrClosePipeLineEffect,
    /// <summary>
    /// 给ui界面设置阀门，仪表等的信息,并发给服务端
    /// </summary>
    SetControllerPartInfo,
    /// <summary>
    /// 给ui界面设置数学模型的设定值,并发给服务端
    /// </summary>
    SetAlgorithmModelData,
    /// <summary>
    /// 阀门的数据变动，需要转动阀门（如：读取数据等）
    /// </summary>
    ValveHaveChanged,
    /// <summary>
    /// 读取进度的时候会有仪表的数据变动需要转动仪表的指针或者led显示
    /// </summary>
    MeterDataChanged,
    /// <summary>
    /// 设置阀门的位置和名称
    /// </summary>
    SetValveLocationAndName,
    /// <summary>
    /// 设置仪表的位置和名称
    /// </summary>
    SetMeterLocationAndName,
    /// <summary>
    /// 隐藏工具箱的图片
    /// </summary>
    HideSprite,
    /// <summary>
    /// 播放视频
    /// </summary>
    PlayVideo,
    /// <summary>
    /// 添加一个数据表
    /// </summary>
    ShowDataSheet,
    /// <summary>
    /// 隐藏一个数据表
    /// </summary>
    HideDataSheet,
    /// <summary>
    /// 打开一个装置的操作
    /// </summary>
    EmployProcess,
    /// <summary>
    /// 关闭一个装置的操作
    /// </summary>
    PauseEmployProcess,
    /// <summary>
    /// 装置上的数据发生变化UI层
    /// </summary>
    UpdateProcessData,
    /// <summary>
    /// 给大弹窗设置具体数据
    /// </summary>
    SetDeviceData,
    /// <summary>
    /// 设置数据并大屏展示
    /// </summary>
    SetDataAndLargeScreenDisplay,
    /// <summary>
    /// 给页面设置工艺，告知这是属于那个工艺的
    /// </summary>
    SetProcess,
    /// <summary>
    /// 打开某个仪表实时折线图
    /// </summary>
    OpenRealTimeLineChart,
    /// <summary>
    /// 添加输入框的回调
    /// </summary>
    CloseParameterInputCallBackAction,
    /// <summary>
    /// 设置保存记录 读取记录是那个装置的
    /// </summary>
    SetRecordProcess,
    /// <summary>
    /// 展示错误提示
    /// </summary>
    ShowErrorMessage,
    /// <summary>
    /// 展示一个近景仪表
    /// </summary>
    ShowOneShortMeter,
    /// <summary>
    /// 隐藏所有的近景仪表
    /// </summary>
    HideAllCloseShotMeter,
    /// <summary>
    /// 根据装置名称创建扰动页面
    /// </summary>
    CreateDisturbForm,
    /// <summary>
    /// 根据装置名称创建组态设置页面
    /// </summary>
    CreateConfigurationForm,
    /// <summary>
    /// 设置无信号仪表
    /// </summary>
    SetNoSignalMeter,
    /// <summary>
    /// 设置需要弹窗展示的仪表
    /// </summary>
    SetNeedShowMeter
}

[Flags]
public enum InstrumentName
{

}