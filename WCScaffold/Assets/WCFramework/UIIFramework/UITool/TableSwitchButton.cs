﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TableSwitchButton : MonoBehaviour
{
    private EventSystem system;

    private Dictionary<int, GameObject> inputFieldGroup;

    private int index;

    void Start()
    {
        system = EventSystem.current;
        inputFieldGroup = new Dictionary<int, GameObject>();
        index = 0;
        for (int i = 0; i < transform.childCount; i++)
        {
            inputFieldGroup.Add(i, transform.GetChild(i).gameObject);
        }

        GameObject obj;
        inputFieldGroup.TryGetValue(index, out obj);
        system.SetSelectedGameObject(obj, new BaseEventData(system));

    }

    // Update is called once per frame
    void Update()
    {

        if (system.currentSelectedGameObject != null && Input.GetKeyDown(KeyCode.Tab))
        {
            GameObject currentObject = system.currentSelectedGameObject;
            foreach (KeyValuePair<int, GameObject> item in inputFieldGroup)
            {
                if (item.Value == currentObject)
                {
                    index = item.Key + 1;
                    if (index == inputFieldGroup.Count)
                    {
                        index = 0;
                    }
                    break;
                }
            }
            GameObject obj;
            inputFieldGroup.TryGetValue(index, out obj);
            system.SetSelectedGameObject(obj, new BaseEventData(system));
        }

    }
}
