﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HoveHight : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    public Image hoveImage;
    public Image selectImage;

    public Color hoveColor = new Color(0, 0, 0, 50);
    public Color normalColor = new Color(0, 0, 0, 0);

    private bool isHover = false;

    private void OnEnable()
    {
        if (hoveImage != null)
        {
            hoveImage.gameObject.SetActive(false);
        }
        selectImage.gameObject.SetActive(false);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isHover = true;
        SetHoverImage();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isHover = false;
        SetNormalImage();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        selectImage.gameObject.SetActive(true);
        if (isHover)
        {
            SetNormalImage();
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        selectImage.gameObject.SetActive(false);
        if (isHover)
        {
            SetHoverImage();
        }
    }

    /// <summary>
    /// 图片设置悬停状态
    /// </summary>
    private void SetHoverImage()
    {
        if (hoveImage == null)
        {
            GetComponent<Image>().color = hoveColor;
        }
        else
        {
            hoveImage.gameObject.SetActive(true);
        }
    }

    /// <summary>
    /// 图片设置普通状态
    /// </summary>
    private void SetNormalImage()
    {
        if (hoveImage == null)
        {
            GetComponent<Image>().color = normalColor;

        }
        else
        {
            hoveImage.gameObject.SetActive(false);
        }
    }

}
