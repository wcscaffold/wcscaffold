﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UITool : MonoBehaviour
{
    public static Camera uiCamera;

    public static RectTransform rectTransform;
    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        uiCamera = transform.Find("UICamera").GetComponent<Camera>();

        DontDestroyOnLoad(this);

    }

    /// <summary>
    /// 查找子节点对象
    /// 内部使用“递归算法”
    /// </summary>
    /// <param name="goParent">父对象</param>
    /// <param name="chiildName">查找的子对象名称</param>
    /// <returns></returns>
    public static Transform FindTheChildNode(GameObject goParent, string chiildName)
    {
        Transform searchTrans = null;                   //查找结果

        searchTrans = goParent.transform.Find(chiildName);
        if (searchTrans == null)
        {
            foreach (Transform trans in goParent.transform)
            {
                searchTrans = FindTheChildNode(trans.gameObject, chiildName);
                if (searchTrans != null)
                {
                    return searchTrans;

                }
            }
        }
        return searchTrans;
    }

    /// <summary>
    /// 屏幕坐标转换成UGUI的坐标
    /// canvas的render mode设置成camera时,(0,0)在屏幕中间
    /// </summary>
    public static Vector2 ScreenPosToUIPos(Vector2 screenPos, Transform targetTransform = null)
    {
        RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, screenPos, uiCamera, out Vector2 localPoint);
        if (targetTransform == null)
        {
            return localPoint;
        }
        else
        {
            return localPoint + new Vector2(targetTransform.GetComponent<RectTransform>().rect.width / 2, targetTransform.GetComponent<RectTransform>().rect.height / 2);
        }
    }
}
