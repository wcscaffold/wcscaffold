﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public enum MoveUIType
{
    None, Self

}
public class CanMoveUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler//IDragHandler, IBeginDragHandler, IEndDragHandler
{
    public Image img;//实例化后的对象
    private Vector3 offPos = Vector3.zero;//存储按下鼠标时的图片-鼠标位置差
                                          // private Vector3 arragedPos; //保存经过整理后的向量，用于图片移动
    private bool isOver = false;
    private bool isMoving = false;

    private bool isCanMove = false;

    public MoveUIType uIType = MoveUIType.None;

    private void Update()
    {
        if (isOver && Input.GetMouseButtonDown(0))
        {
            isCanMove = true;
        }
        if (isCanMove || isMoving)
        {
            if (uIType != MoveUIType.Self)
            {
                img.transform.parent.SetSiblingIndex(99);
                img.transform.parent.parent.SetSiblingIndex(99);
            }
            else
            {
                img.transform.SetSiblingIndex(99);
                img.transform.parent.SetSiblingIndex(99);
            }

            if (offPos == Vector3.zero)
            {
                Vector3 arragedPos = UITool.ScreenPosToUIPos(Input.mousePosition);
                offPos = img.transform.localPosition - arragedPos;
            }
            img.transform.localPosition = offPos + new Vector3(UITool.ScreenPosToUIPos(Input.mousePosition).x, UITool.ScreenPosToUIPos(Input.mousePosition).y);
            isMoving = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            isMoving = false;
            isCanMove = false;
            offPos = Vector3.zero;
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isOver = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isOver = false;
    }


    //public void OnDrag(PointerEventData eventData)
    //{
    //    img.transform.localPosition = offPos + new Vector3(UITool.ScreenPosToUIPos(Input.mousePosition).x,
    //                                                  UITool.ScreenPosToUIPos(Input.mousePosition).y);
    //    //Debug.LogError("img  : " + img.transform.position);
    //    //Debug.LogError("mousePosition  : " + UITool.ScreenPosToUIPos(Input.mousePosition));
    //}

    //public void OnBeginDrag(PointerEventData eventData)
    //{

    //    Vector3 arragedPos = UITool.ScreenPosToUIPos(Input.mousePosition);

    //    offPos = img.transform.localPosition - arragedPos;
    //    Debug.LogError("img  : " + img.transform.position);
    //    Debug.LogError("offPos : " + offPos);
    //    Debug.LogError("mousePosition  : " + UITool.ScreenPosToUIPos(Input.mousePosition));
    //}

    //public void OnEndDrag(PointerEventData eventData)
    //{
    //    img.transform.localPosition = offPos + new Vector3(UITool.ScreenPosToUIPos(Input.mousePosition).x,
    //                                                  UITool.ScreenPosToUIPos(Input.mousePosition).y);
    //}
}
