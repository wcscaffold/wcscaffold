﻿using FrameworkDesign;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface IUIMaskMgrSystem : ISystem
{

    /// <summary>
    /// 设置遮罩状态
    /// </summary>
    /// <param name="goDisplayUIForms"> 需要显示的form </param>
    /// <param name="lucenyType"> 设置透明度 </param>
    void SetMaskWindow(GameObject goDisplayUIForms, UIFormLucencyType lucenyType = UIFormLucencyType.Lucency);

    /// <summary>
    /// 取消遮罩
    /// </summary>
    void CancelMaskWindow();
}


/// <summary>
/// ui遮罩
/// </summary>
public class UIMaskMgrSystem : AbstractSystem, IUIMaskMgrSystem
{
    private Camera uiCamera;
    private GameObject topPanel;
    private GameObject maskPanel;
    //ui相机初始景深
    private float originalUICameraDepth;

    protected override void OnInit()
    {
        topPanel = GameObject.FindGameObjectWithTag(UIDefine.SYS_TAG_CANVAS);
        maskPanel = topPanel.transform.Find(UIDefine.SYS_FORM_POPUP).Find(UIDefine.SYS_PANEL_MASK).gameObject;
        maskPanel.SetActive(false);
        uiCamera = GameObject.FindGameObjectWithTag(UIDefine.SYS_TAG_UICAMERA).GetComponent<Camera>();
        originalUICameraDepth = uiCamera.depth;
    }

    /// <summary>
    /// 设置遮罩状态
    /// </summary>
    /// <param name="goDisplayUIForms"> 需要显示的form </param>
    /// <param name="lucenyType"> 设置透明度 </param>
    public void SetMaskWindow(GameObject goDisplayUIForms, UIFormLucencyType lucenyType = UIFormLucencyType.Lucency)
    {
        topPanel.transform.SetAsLastSibling();
        switch (lucenyType)
        {
            case UIFormLucencyType.Lucency:
                maskPanel.SetActive(true);
                Color newColor1 = new Color(UIDefine.SYS_UIMASK_LUCENCY_COLOR_RGB, UIDefine.SYS_UIMASK_LUCENCY_COLOR_RGB,
                                            UIDefine.SYS_UIMASK_LUCENCY_COLOR_RGB, UIDefine.SYS_UIMASK_LUCENCY_COLOR_RGB_A);
                maskPanel.GetComponent<Image>().color = newColor1;
                break;
            case UIFormLucencyType.Translucence:
                maskPanel.SetActive(true);
                Color newColor2 = new Color(UIDefine.SYS_UIMASK_TRANS_LUCENCY_COLOR_RGB, UIDefine.SYS_UIMASK_TRANS_LUCENCY_COLOR_RGB,
                                            UIDefine.SYS_UIMASK_TRANS_LUCENCY_COLOR_RGB, UIDefine.SYS_UIMASK_TRANS_LUCENCY_COLOR_RGB_A);
                maskPanel.GetComponent<Image>().color = newColor2;
                break;
            case UIFormLucencyType.ImPenetrable:
                maskPanel.SetActive(true);
                Color newColor3 = new Color(UIDefine.SYS_UIMASK_IMPENETRABLE_COLOR_RGB, UIDefine.SYS_UIMASK_IMPENETRABLE_COLOR_RGB,
                                            UIDefine.SYS_UIMASK_IMPENETRABLE_COLOR_RGB, UIDefine.SYS_UIMASK_IMPENETRABLE_COLOR_RGB_A);
                maskPanel.GetComponent<Image>().color = newColor3;
                break;
            case UIFormLucencyType.Pentrate:
                //检测遮罩这一层级是否活跃
                if (maskPanel.activeInHierarchy)
                {
                    maskPanel.SetActive(false);
                }
                break;
        }
        maskPanel.transform.SetAsLastSibling();
        goDisplayUIForms.transform.SetAsLastSibling();

        //增加当前UI摄像机的层深（保证当前摄像机为最前显示）
        if (uiCamera != null)
        {
            uiCamera.depth = uiCamera.depth + 100;    //增加层深
        }
    }

    /// <summary>
    /// 取消遮罩
    /// </summary>
    public void CancelMaskWindow()
    {
        topPanel.transform.SetAsFirstSibling();

        //检测遮罩这一层级是否活跃
        if (maskPanel.activeInHierarchy)
        {
            maskPanel.SetActive(false);
        }

        //恢复层深
        if (uiCamera != null)
        {
            uiCamera.depth = originalUICameraDepth;
        }
    }


}
