﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;
using UnityEngine.UI;
using System;
using TMPro;

public enum ContentType
{
    X, Y
}

public interface IUICloneObjSystem : ISystem
{
    /// <summary>
    /// 克隆2个物体
    /// </summary>
    List<GameObject[]> CloneContentTwoGameObj(List<string[]> cloneInfoList, GameObject[] template, Transform content,
               ContentType contentType, Action<GameObject, bool> buttonAction = null);


    /// <summary>
    /// 获取加载的图片
    /// </summary>
    Sprite GetImageSprite(string path);

    /// <summary>
    /// 克隆物体，并设置好层级关系
    /// </summary>
    List<GameObject> CloneGameObj(List<string> cloneInfoList, GameObject template, Action<GameObject, bool> buttonAction = null);

    /// <summary>
    /// 克隆物体，并设置content长度或高度
    /// </summary>
    List<GameObject> CloneContentGameObj(List<string> cloneInfoList, GameObject template, Transform content,
                                         ContentType contentType, Action<GameObject, bool> buttonAction = null);
}

public class UICloneObjSystem : AbstractSystem, IUICloneObjSystem
{
    protected override void OnInit()
    {

    }

    public List<GameObject> CloneGameObj(List<string> cloneInfoList, GameObject template, Action<GameObject, bool> buttonAction = null)
    {
        List<GameObject> cloneObjs = new List<GameObject>();
        for (int i = 0; i < cloneInfoList.Count; i++)
        {
            GameObject newSonSelectButton = UnityEngine.Object.Instantiate(template);
            cloneObjs.Add(newSonSelectButton);
            newSonSelectButton.name = cloneInfoList[i];
            if (newSonSelectButton.transform.GetComponentInChildren<Text>() != null)
            {
                newSonSelectButton.transform.GetComponentInChildren<Text>().text = this.GetSystem<IMultiLanguageSystem>().GetText(cloneInfoList[i]);
            }
            else if (newSonSelectButton.transform.GetComponentInChildren<TextMeshProUGUI>() != null)
            {
                newSonSelectButton.transform.GetComponentInChildren<TextMeshProUGUI>().text = this.GetSystem<IMultiLanguageSystem>().GetText(cloneInfoList[i]);
            }
            newSonSelectButton.transform.SetParent(template.transform.parent);
            newSonSelectButton.GetComponent<RectTransform>().localScale = Vector3.one;

            if (buttonAction != null)
            {
                newSonSelectButton.GetComponent<WCBaseButton>().onValueChanged = buttonAction;
            }
            newSonSelectButton.SetActive(true);
        }
        return cloneObjs;
    }

    public List<GameObject> CloneContentGameObj(List<string> cloneInfoList, GameObject template, Transform content,
        ContentType contentType, Action<GameObject, bool> buttonAction = null)
    {
        List<GameObject> cloneObjs = new List<GameObject>();
        for (int i = 0; i < cloneInfoList.Count; i++)
        {
            GameObject newSonSelectButton = UnityEngine.Object.Instantiate(template);
            cloneObjs.Add(newSonSelectButton);
            newSonSelectButton.name = cloneInfoList[i];
            if (newSonSelectButton.transform.GetComponentInChildren<Text>() != null)
            {
                string str = this.GetSystem<IMultiLanguageSystem>().GetText(cloneInfoList[i]);
                if (!string.IsNullOrEmpty(str))
                {
                    newSonSelectButton.transform.GetComponentInChildren<Text>().text = str;
                }
                else
                {
                    newSonSelectButton.transform.GetComponentInChildren<Text>().text = cloneInfoList[i];
                }
            }
            newSonSelectButton.transform.SetParent(content);
            newSonSelectButton.GetComponent<RectTransform>().localScale = Vector3.one;

            if (buttonAction != null)
            {
                newSonSelectButton.GetComponent<WCBaseButton>().onValueChanged = buttonAction;
            }
            newSonSelectButton.SetActive(true);
        }
        ExpandContent(cloneInfoList.Count, contentType, content);
        return cloneObjs;
    }


    public List<GameObject[]> CloneContentTwoGameObj(List<string[]> cloneInfoList, GameObject[] template, Transform content,
           ContentType contentType, Action<GameObject, bool> buttonAction = null)
    {
        List<GameObject[]> cloneObjs = new List<GameObject[]>();
        for (int i = 0; i < cloneInfoList.Count; i++)
        {
            GameObject[] gameObjects = new GameObject[2];
            GameObject newSonSelectButton1 = UnityEngine.Object.Instantiate(template[0]);
            GameObject newSonSelectButton2 = UnityEngine.Object.Instantiate(template[1]);

            gameObjects[0] = newSonSelectButton1;
            gameObjects[1] = newSonSelectButton2;

            newSonSelectButton1.name = cloneInfoList[i][0] + "-" + i;
            newSonSelectButton2.name = cloneInfoList[i][0] + "-" + i;


            newSonSelectButton1.transform.SetParent(content);
            newSonSelectButton1.GetComponent<RectTransform>().localScale = Vector3.one;
            newSonSelectButton2.transform.SetParent(content);
            newSonSelectButton2.GetComponent<RectTransform>().localScale = Vector3.one;

            if (buttonAction != null)
            {
                newSonSelectButton1.GetComponent<WCBaseButton>().onValueChanged = buttonAction;
            }
            newSonSelectButton1.SetActive(true);
            newSonSelectButton1.SetActive(true);
            cloneObjs.Add(gameObjects);
        }
        ExpandContent(cloneInfoList.Count * 2, contentType, content);
        return cloneObjs;
    }

    /// <summary>
    /// 自动扩充content的高度或宽度
    /// </summary>
    private void ExpandContent(int count, ContentType contentType, Transform content)
    {
        float spacing = 0;
        float cellSize = 0;
        switch (contentType)
        {
            case ContentType.X:
                spacing = content.GetComponent<GridLayoutGroup>().spacing.x;
                cellSize = content.GetComponent<GridLayoutGroup>().cellSize.x;
                content.GetComponent<RectTransform>().sizeDelta =
                    new Vector2(count * (spacing + cellSize) - spacing, content.GetComponent<RectTransform>().sizeDelta.y);
                break;
            case ContentType.Y:
                spacing = content.GetComponent<GridLayoutGroup>().spacing.y;
                cellSize = content.GetComponent<GridLayoutGroup>().cellSize.y;
                content.GetComponent<RectTransform>().sizeDelta =
                    new Vector2(content.GetComponent<RectTransform>().sizeDelta.x, count * (spacing + cellSize) - spacing);
                break;
        }
    }

    public Sprite GetImageSprite(string path)
    {
        return this.GetUtility<IObjectOperation>().LoadResource<Sprite>(path, true);
    }

}
