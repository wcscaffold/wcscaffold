﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;
public interface IUIManagerSystem : ISystem
{
    /// <summary>
    /// 通过ui类型打开对应ui
    /// </summary>
    BaseUIForm ShowUIForm<T>(SceneName sceneName) where T : BaseUIForm;

    /// <summary>
    /// 通过字符串打开对应ui
    /// </summary>
    BaseUIForm ShowUIForm(SceneName sceneName, string formName);

    void CloseUIForm<T>() where T : BaseUIForm;

    void CloseAllUIForm();

}
public class UIManagerSystem : AbstractSystem, IUIManagerSystem
{
    //缓存所有的ui
    private Dictionary<string, BaseUIForm> allUIFormDic;
    //当前显示的ui
    private Dictionary<string, BaseUIForm> currentFormDic;
    //定义“栈”集合,存储显示当前所有[反向切换]的窗体类型
    private Stack<BaseUIForm> currentUIFormsStack;

    //各种类型窗体的节点
    private Transform canvasTransform;
    private Transform normalTransform;
    private Transform fixedTransform;
    private Transform popUpTransform;

    protected override void OnInit()
    {
        allUIFormDic = new Dictionary<string, BaseUIForm>();
        currentFormDic = new Dictionary<string, BaseUIForm>();
        currentUIFormsStack = new Stack<BaseUIForm>();

        InitCanvas();
        //获取各个节点的transform
        canvasTransform = GameObject.FindGameObjectWithTag(UIDefine.SYS_TAG_CANVAS).transform;
        normalTransform = canvasTransform.Find(UIDefine.SYS_FORM_NORMAL);
        fixedTransform = canvasTransform.Find(UIDefine.SYS_FORM_FIXED);
        popUpTransform = canvasTransform.Find(UIDefine.SYS_FORM_POPUP);
    }

    /// <summary>
    /// 实例canvas
    /// </summary>
    private void InitCanvas()
    {
        this.GetUtility<IObjectOperation>().InsAsset(UIDefine.SYS_PATH_CANVAS, true);
    }

    /// <summary>
    /// 根据form名称 显示对应的from
    /// </summary>
    public BaseUIForm ShowUIForm<T>(SceneName sceneName) where T : BaseUIForm
    {
        string formName = typeof(T).Name;
        return ShowUIForm(sceneName, formName);
    }

    public BaseUIForm ShowUIForm(SceneName sceneName, string formName)
    {
        BaseUIForm baseUIForm = null;
        baseUIForm = LoadFormToAllForm(formName, "UIPrefab/" + sceneName.ToString() + "/" + formName);

        if (baseUIForm == null) return null;

        switch (baseUIForm.CurrentUIType.UIForms_ShowMode)
        {
            case UIFormShowMode.HideOther:
                ShowHideOtherForm(formName, baseUIForm);
                break;
            case UIFormShowMode.Normal:
                ShowNoramlForm(formName, baseUIForm);
                break;
            case UIFormShowMode.ReverseChange:
                ShowReverseChangeForm(formName, baseUIForm);
                break;
        }
        return baseUIForm;
    }

    /// <summary>
    /// 根据form名称 关闭对应的from
    /// </summary>
    public void CloseUIForm<T>() where T : BaseUIForm
    {
        string formName = typeof(T).Name;
        BaseUIForm baseUIForm = null;
        allUIFormDic.TryGetValue(formName, out baseUIForm);

        if (baseUIForm == null) return;

        switch (baseUIForm.CurrentUIType.UIForms_ShowMode)
        {
            case UIFormShowMode.HideOther:
                CloseHideOtherForm(formName, baseUIForm);
                break;
            case UIFormShowMode.Normal:
                CloseNormalForm(formName, baseUIForm);
                break;
            case UIFormShowMode.ReverseChange:
                CloseReverseChangeForm();
                break;
        }
    }

    /// <summary>
    ///  关闭所有的from
    /// </summary>
    public void CloseAllUIForm()
    {
        foreach (KeyValuePair<string, BaseUIForm> kv in allUIFormDic)
        {
            kv.Value.Hiding();
        }
        currentFormDic.Clear();
        currentUIFormsStack.Clear();
    }


    /// <summary>
    /// 加载form,并将其加入所有form的缓存中
    /// </summary>
    private BaseUIForm LoadFormToAllForm(string formName, string formPath)
    {
        BaseUIForm baseUIForm = null;
        allUIFormDic.TryGetValue(formName, out baseUIForm);
        if (baseUIForm == null)
        {
            baseUIForm = LoadUIForm(formName, formPath);
        }
        return baseUIForm;
    }

    /// <summary>
    /// 加载form,并暂时隐藏
    /// </summary>
    private BaseUIForm LoadUIForm(string formName, string formPath)
    {
        GameObject uiPrefab = null;
        BaseUIForm baseUIForm;
        if (!string.IsNullOrEmpty(formPath))
        {
            uiPrefab = this.GetUtility<IObjectOperation>().InsAsset(formPath, true);
        }
        if (canvasTransform != null && uiPrefab != null)
        {
            baseUIForm = uiPrefab.AddComponent(Type.GetType(formName)) as BaseUIForm;
            if (baseUIForm == null)
            {
                Debug.LogError("baseUIForm==null! ,请先确认窗体预设是否存在对应脚本,formName=" + formName);
            }
            switch (baseUIForm.CurrentUIType.UIForms_Type)
            {
                case UIFormType.Fixed:
                    baseUIForm.transform.SetParent(fixedTransform, false);
                    break;
                case UIFormType.Normal:
                    baseUIForm.transform.SetParent(normalTransform, false);
                    break;
                case UIFormType.PopUp:
                    baseUIForm.transform.SetParent(popUpTransform, false);
                    break;
            }
            // uiPrefab.SetActive(false);
            allUIFormDic.Add(formName, baseUIForm);
            return baseUIForm;
        }
        else
        {
            Debug.LogError("canvasTransform==null || uiPrefab==null 请检查 formName=" + formName);
        }
        Debug.LogError(GetType() + "出现不可预知的错误");
        return null;
    }

    /// <summary>
    /// 显示 普通 form,并将其加入显示中form的缓存中
    /// </summary>
    private void ShowNoramlForm(string formName, BaseUIForm baseUIForm)
    {
        BaseUIForm currentUIForm = null;
        currentFormDic.TryGetValue(formName, out currentUIForm);

        if (currentUIForm != null) return;
        currentFormDic.Add(formName, baseUIForm);
        baseUIForm.Display();
    }

    /// <summary>
    /// 显示 反向切换 form 并入栈
    /// </summary>
    private void ShowReverseChangeForm(string formName, BaseUIForm baseUIForm)
    {
        if (currentUIFormsStack.Count > 0)
        {
            currentUIFormsStack.Peek().Freeze();
        }

        if (baseUIForm != null)
        {
            if (!currentUIFormsStack.Contains(baseUIForm))
            {
                currentUIFormsStack.Push(baseUIForm);
            }
            baseUIForm.Display();
        }
        else
        {
            Debug.LogError("PushUIFormToStack baseUIForm为空 请检查 formName" + formName);
        }
    }

    /// <summary>
    /// 显示 隐藏其他 form 并将正在显示的ui全部隐藏
    /// </summary>
    private void ShowHideOtherForm(string formName, BaseUIForm baseUIForm)
    {
        if (string.IsNullOrEmpty(formName)) return;

        BaseUIForm currentUIForm = null;
        currentFormDic.TryGetValue(formName, out currentUIForm);
        if (currentUIForm != null) return;

        foreach (KeyValuePair<string, BaseUIForm> kv in currentFormDic)
        {
            kv.Value.Hiding();
        }

        foreach (BaseUIForm stackUI in currentUIFormsStack)
        {
            stackUI.Hiding();
        }

        currentFormDic.Add(formName, baseUIForm);
        baseUIForm.Display();
    }

    /// <summary>
    /// 关闭 反向切换 form 并出栈
    /// </summary>
    private void CloseReverseChangeForm()
    {
        if (currentUIFormsStack.Count > 1)
        {
            currentUIFormsStack.Pop().Hiding();
            if (currentUIFormsStack.Count > 0)
            {
                currentUIFormsStack.Peek().ReDisplay();
            }
        }
        else if (currentUIFormsStack.Count == 1)
        {
            currentUIFormsStack.Pop().Hiding();
        }
    }

    /// <summary>
    /// 关闭 普通  UIForm 并移出该form在显示中form的缓存
    /// </summary>
    private void CloseNormalForm(string formName, BaseUIForm baseUIForm)
    {
        baseUIForm.Hiding();
        currentFormDic.Remove(formName);
    }

    /// <summary>
    /// 关闭 隐藏其他  UIForm 并显示需要显示的ui
    /// </summary>
    private void CloseHideOtherForm(string formName, BaseUIForm baseUIForm)
    {
        if (string.IsNullOrEmpty(formName)) return;

        BaseUIForm currentUIForm = null;
        currentFormDic.TryGetValue(formName, out currentUIForm);
        if (currentUIForm == null) return;

        baseUIForm.Hiding();
        currentFormDic.Remove(formName);

        foreach (BaseUIForm currentUI in currentFormDic.Values)
        {
            currentUI.ReDisplay();
        }
        foreach (BaseUIForm stackUI in currentUIFormsStack)
        {
            stackUI.ReDisplay();
        }
    }
}
