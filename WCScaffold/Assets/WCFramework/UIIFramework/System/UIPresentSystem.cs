﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;
using System;
using UnityEngine.UI;
/// <summary>
/// 用于存放资源的
/// </summary>
public enum SceneName
{
    Common,
    HomePage
}

public enum GameSceneName
{
    /// <summary>
    /// 登录场景
    /// </summary>
    Login,
    /// <summary>
    /// 产品介绍场景
    /// </summary>
    ProjectIntroduction,
    /// <summary>
    /// 数据仿真场景
    /// </summary>
    DataSimulation,
    /// <summary>
    /// 结构原理场景
    /// </summary>
    OpenStructurePrinciple,
    /// <summary>
    /// 拆装场景
    /// </summary>
    SplitEquipment,
    /// <summary>
    /// 模拟考核场景
    /// </summary>
    SimulationAssessment,
    /// <summary>
    /// 模拟练习场景
    /// </summary>
    SimulationExercise,
}

public interface IUIPresentSystem : ISystem
{
    void ReturnLastView();
}

public class UIPresentSystem : AbstractSystem, IUIPresentSystem
{
    private Stack<GameSceneName> uIFormPresentsStack = new Stack<GameSceneName>();
    private IUIManagerSystem iuiManagerSystem;

    protected override void OnInit()
    {
        this.GetModel<IGameModel>().SceneName.OnValueChanged += OpenFormByCurrentType;
        iuiManagerSystem = this.GetSystem<IUIManagerSystem>();
    }

    /// <summary>
    /// 根据当前场景类型打开对应的页面
    /// </summary>
    private void OpenFormByCurrentType(string sceneName)
    {

    }

    /// <summary>
    /// 返回上一个页面
    /// </summary>
    public void ReturnLastView()
    {
        Debug.Log(uIFormPresentsStack.Pop());
        GameSceneName sceneName = uIFormPresentsStack.Peek();
        this.GetModel<IGameModel>().SceneName.Value = sceneName.ToString();
        uIFormPresentsStack.Pop();
    }
}