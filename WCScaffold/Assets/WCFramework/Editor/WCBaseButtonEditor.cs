﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(WCBaseButton), false)]
public class WCBaseButtonEditor : Editor
{
    SerializedProperty wcButtonType;
    private void OnEnable()
    {
    }

    public override void OnInspectorGUI()
    {
        wcButtonType = serializedObject.FindProperty("wcButtonType");

        EditorGUILayout.PropertyField(wcButtonType);

        EditorGUILayout.PropertyField(serializedObject.FindProperty("interactable"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("isCanRepeatClick"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("isOn"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("isCanSelect"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("buttonGroup"));
        switch ((WCButtonType)wcButtonType.intValue)
        {
            case WCButtonType.CommonImageButton:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("buttonImage"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("normalTexture"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("selectTexture"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hoverTexture"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("pressTexture"));

                EditorGUILayout.PropertyField(serializedObject.FindProperty("normalColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("selectColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hoverColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("pressColor"));

                EditorGUILayout.PropertyField(serializedObject.FindProperty("tipsObj"));

                break;
            case WCButtonType.CommonTextButton:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("buttonText"));

                EditorGUILayout.PropertyField(serializedObject.FindProperty("normalColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("selectColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hoverColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("pressColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("selectImageObj"));
                break;
            case WCButtonType.TextAndImageButton:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("buttonImage"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("buttonText"));

                EditorGUILayout.PropertyField(serializedObject.FindProperty("normalTexture"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("selectTexture"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hoverTexture"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("pressTexture"));

                EditorGUILayout.PropertyField(serializedObject.FindProperty("normalColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("selectColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hoverColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("pressColor"));
                break;
            case WCButtonType.AllColorButton:
                EditorGUILayout.PropertyField(serializedObject.FindProperty("buttonImage"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("buttonText"));

                EditorGUILayout.PropertyField(serializedObject.FindProperty("normalColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("selectColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("hoverColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("pressColor"));

                EditorGUILayout.PropertyField(serializedObject.FindProperty("imageNormalColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("imageSelectColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("imageHoverColor"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("imagePressColor"));

                EditorGUILayout.PropertyField(serializedObject.FindProperty("tipsObj"));
                break;
        }
        serializedObject.ApplyModifiedProperties();

    }
}
