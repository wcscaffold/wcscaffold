﻿using Excel;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ExcelConfig
{
    /// <summary>
    /// 存放excel表文件夹的的路径,本例xecel表放在了"Assets/Excels/"当中
    /// </summary>
    public static readonly string excelsFolderPath = Application.dataPath + "/Excels/";

    /// <summary>
    /// 存放Excel转化CS文件的文件夹路径
    /// </summary>
    public static readonly string assetPath = "Assets/Resources/";

}
public class ReadExcel
{
    [MenuItem("ReadExcel/ReadExcelToAsset", false, 1)]
    static void ReadExcelToAsset()
    {
        //ItemManager itemManager = ScriptableObject.CreateInstance<ItemManager>();
        //itemManager.dataArray = ReadnfoExcelTool<Item>(Directory.GetCurrentDirectory() + "/Assets/Editor/信息表.xlsx", "Sheet1", new List<int> { 5, 8, 10 });
        //SaveAssets(itemManager, "ItemAsset");


        //AssessmentDataManager assessmentDataManager = ScriptableObject.CreateInstance<AssessmentDataManager>();
        //assessmentDataManager.dataArray = ReadnfoExcelTool<AssessmentData>(Directory.GetCurrentDirectory() + "/Assets/Editor/信息表.xlsx", "Sheet2", new List<int> { 5 });
        //SaveAssets(assessmentDataManager, "AssessmentDataAsset");

        MultiLanguageManager multiLanguageManager = ScriptableObject.CreateInstance<MultiLanguageManager>();
        multiLanguageManager.dataArray = ReadnfoExcelTool<MultiLanguageItem>(Directory.GetCurrentDirectory() + "/Assets/Editor/信息表.xlsx", "Sheet3");
        SaveAssets(multiLanguageManager, "MultiLanguageItemAsset");
    }

    private static void SaveAssets(ScriptableObject scriptableObject, string savePath)
    {
        //asset文件的路径 要以"Assets/..."开始,否则CreateAsset会报错
        string lineInfoItemPath = string.Format("{0}{1}.asset", ExcelConfig.assetPath, savePath);
        //生成一个Asset文件
        AssetDatabase.CreateAsset(scriptableObject, lineInfoItemPath);
        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
    }


    static T[] ReadnfoExcelTool<T>(string excelpath, string sheetname, List<int> intData = null) where T : BaseItem
    {
        FileStream fileStream = File.Open(excelpath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        ExcelPackage pck = new ExcelPackage(new MemoryStream(), fileStream);
        ExcelWorkbook ewb = pck.Workbook;
        ExcelWorksheet ew = ewb.Worksheets[sheetname];

        int columnNum = ew.Dimension.End.Column;//获取列数   
        int rowNum = ew.Dimension.End.Row;//获取行数


        T[] tArray = new T[rowNum - 1];
        Type itemTyep = typeof(T);

        for (int i = 2; i <= rowNum; i++)
        {
            T item = System.Activator.CreateInstance<T>();
            for (int j = 1; j <= columnNum; j++)
            {
                string a = ew.Cells[1, j].Value.ToString();
                if (intData != null && intData.Contains(j))
                {
                    itemTyep.GetProperty(a).SetValue(item, float.Parse(ew.Cells[i, j].Value.ToString()));

                }
                else
                {
                    if (ew.Cells[i, j].Value == null)
                    {
                        ew.Cells[i, j].Value = "";
                    }

                    itemTyep.GetProperty(a).SetValue(item, ew.Cells[i, j].Value.ToString());
                }
            }
            tArray[i - 2] = item;
        }
        return tArray;
    }
}

