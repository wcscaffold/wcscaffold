﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FrameworkDesign
{
    public interface IArchitecture
    {
        /// <summary>
        /// 注册系统
        /// </summary>
        void RegisterSystem<T>(T instance) where T : ISystem;

        /// <summary>
        /// 注册model
        /// </summary>
        void RegisterModel<T>(T instance) where T : IModel;

        /// <summary>
        /// 注册工具
        /// </summary>
        void RegisterUtility<T>(T instance);

        /// <summary>
        /// 获取工具
        /// </summary>
        T GetUtility<T>() where T : class;

        /// <summary>
        /// 获取system
        /// </summary>
        T GetSystem<T>() where T : class, ISystem;

        /// <summary>
        /// 获取 Model
        /// </summary>
        T GetModel<T>() where T : class, IModel;

        /// <summary>
        /// 发送命令
        /// </summary>
        void SendCommand<T>() where T : ICommand, new();

        /// <summary>
        /// 发送命令
        /// </summary>
        void SendCommand<T>(T command) where T : ICommand;

        /// <summary>
        /// 发送事件
        /// </summary>
        void SendEvent<T>() where T : new();
        /// <summary>
        /// 发送事件
        /// </summary>
        void SendEvent<T>(T e);

        /// <summary>
        /// 注册事件
        /// </summary>
        IUnRegister RegisterEvent<T>(Action<T> onEvent);

        /// <summary>
        /// 注销事件
        /// </summary>
        void UnRegisterEvent<T>(Action<T> onEvent);

    }
    public abstract class Architecture<E> : IArchitecture where E : Architecture<E>, new()
    {
        //类似单例模式
        private static E mArchitecture = null;

        /// <summary>
        /// 接口
        /// </summary>
        public static IArchitecture Interface
        {
            get
            {
                if (mArchitecture == null)
                {
                    MakeSureArchitecture();
                }

                return mArchitecture;
            }
        }

        protected IOCContainer mContainer = new IOCContainer();

        /// <summary>
        /// 注册补丁
        /// </summary>
        public static Action<E> OnRegisterPatch = architecture => { };

        /// <summary>
        /// 是否已经初始化完成
        /// </summary>
        private bool mInited = false;

        /// <summary>
        /// 用于初始化的 Models 的缓存
        /// </summary>
        private List<IModel> mModels = new List<IModel>();

        /// <summary>
        /// 用于初始化的 Systems 的缓存
        /// </summary>
        private List<ISystem> mSystems = new List<ISystem>();



        // 确保 Container 是有实例的
        private static void MakeSureArchitecture()
        {
            if (mArchitecture == null)
            {
                mArchitecture = new E();
                mArchitecture.Init();

                OnRegisterPatch?.Invoke(mArchitecture);

                foreach (IModel architectureModel in mArchitecture.mModels)
                {
                    architectureModel.Init();
                }

                foreach (var architectureSystem in mArchitecture.mSystems)
                {
                    architectureSystem.Init();
                }

                // 清空 Model
                mArchitecture.mModels.Clear();
                // 清空 System
                mArchitecture.mSystems.Clear();

                mArchitecture.mInited = true;
            }
        }

        /// <summary>
        /// 用于注册模块
        /// </summary>
        protected abstract void Init();

        // 提供一个注册模块的 API
        public static void Register<T>(T instance)
        {
            MakeSureArchitecture();
            mArchitecture.mContainer.Register<T>(instance);
        }

        /// <summary>
        /// 注册工具
        /// </summary>
        public void RegisterUtility<T>(T instance)
        {
            mArchitecture.mContainer.Register<T>(instance);
        }

        /// <summary>
        /// 注册系统
        /// </summary>
        public void RegisterSystem<T>(T instance) where T : ISystem
        {
            instance.SetArchitecture(this);
            mContainer.Register<T>(instance);
            if (!mInited)
            {
                instance.Init();
            }
            else
            {
                mSystems.Add(instance);
            }
        }

        /// <summary>
        /// 注册模型
        /// </summary>
        public void RegisterModel<T>(T instance) where T : IModel
        {
            instance.SetArchitecture(this);
            mContainer.Register<T>(instance);
            if (!mInited)
            {
                instance.Init();
            }
            else
            {
                mModels.Add(instance);
            }
        }

        /// <summary>
        /// 获取工具
        /// </summary>
        public T GetUtility<T>() where T : class
        {
            return mContainer.Get<T>();
        }

        /// <summary>
        /// 获取model
        /// </summary>
        public T GetModel<T>() where T : class, IModel
        {
            return mContainer.Get<T>();
        }

        /// <summary>
        /// 获取system
        /// </summary>
        public T GetSystem<T>() where T : class, ISystem
        {
            return mContainer.Get<T>();
        }

        public void SendCommand<T>() where T : ICommand, new()
        {
            var command = new T();
            command.SetArchitecture(this);
            command.Execute();
        }

        public void SendCommand<T>(T command) where T : ICommand
        {
            command.SetArchitecture(this);
            command.Execute();
        }

        private ITypeEventSystem mTypeEventSystem = new TypeEventSystem();

        public void SendEvent<T>() where T : new()
        {
            mTypeEventSystem.Send<T>();
        }

        public void SendEvent<T>(T e)
        {
            mTypeEventSystem.Send<T>(e);
        }

        public IUnRegister RegisterEvent<T>(Action<T> onEvent)
        {
            return mTypeEventSystem.Register<T>(onEvent);
        }

        public void UnRegisterEvent<T>(Action<T> onEvent)
        {
            mTypeEventSystem.UnRegister<T>(onEvent);
        }

    }

}
