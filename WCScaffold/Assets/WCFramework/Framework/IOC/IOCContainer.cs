﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace FrameworkDesign
{
    public class IOCContainer
    {
        public Dictionary<Type, object> mInstanceDic = new Dictionary<Type, object>();

        public void Register<T>(T instance)
        {
            Type key = typeof(T);
            if (mInstanceDic.ContainsKey(key))
            {
                mInstanceDic[key] = instance;
            }
            else
            {
                mInstanceDic.Add(key, instance);
            }
        }

        public T Get<T>() where T : class
        {
            Type key = typeof(T);
            if (mInstanceDic.TryGetValue(key, out object retObj))
            {
                return retObj as T;
            }
            return null;
        }
    }
}

