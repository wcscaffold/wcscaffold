﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
namespace FrameworkDesign
{
    public class Singleton<T> where T : class
    {
        private static T mInstance;
        public static T Instance
        {
            get
            {
                if (mInstance == null)
                {
                    ///获取非public构造
                    ConstructorInfo[] ctors = typeof(T).GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic);
                    ///获取无参数的
                    ConstructorInfo ctor = Array.Find(ctors, p => p.GetParameters().Length == 0);
                    if (ctor == null)
                    {
                        throw new Exception("没有对应的非公共，无参数的构造方法，请检查类型 ：" + typeof(T));
                    }
                    mInstance = ctor.Invoke(null) as T;
                }
                return mInstance;
            }
        }
    }
}