﻿using FrameworkDesign;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface IGameModel : IModel
{
    BindableProperty<string> SceneName { get; }

}
public class GameModel : AbstractModel, IGameModel
{
    protected override void OnInit()
    {

    }

    public BindableProperty<string> SceneName { get; } = new BindableProperty<string>()
    {
        Value = ""
    };
}
