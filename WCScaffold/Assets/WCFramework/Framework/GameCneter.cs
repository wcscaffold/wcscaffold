﻿using FrameworkDesign;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCneter : Architecture<GameCneter>
{
    protected override void Init()
    {

        #region 以下是架构中的内容
        GameObject objectOperation = new GameObject().AddComponent<ObjectOperation>().gameObject;
        objectOperation.name = "ObjectOperation";
        this.RegisterUtility<IObjectOperation>(objectOperation.GetComponent<ObjectOperation>());

        GameObject taskManager = new GameObject().AddComponent<TaskManagerUtility>().gameObject;
        taskManager.name = "TaskManager";
        this.RegisterUtility<ITaskManagerUtility>(objectOperation.GetComponent<TaskManagerUtility>());


        GameObject audioUtility = new GameObject().AddComponent<AudioUtility>().gameObject;
        audioUtility.name = "AudioUtility";
        this.RegisterUtility<IAudioUtility>(audioUtility.GetComponent<AudioUtility>());


        this.RegisterModel<IGameModel>(new GameModel());

        //   this.RegisterSystem<IMultiLanguageSystem>(new MultiLanguageSystem());
        this.RegisterSystem<IUIManagerSystem>(new UIManagerSystem());
        this.RegisterSystem<IUIMaskMgrSystem>(new UIMaskMgrSystem());
        this.RegisterSystem<IUIPresentSystem>(new UIPresentSystem());
        this.RegisterSystem<IUICloneObjSystem>(new UICloneObjSystem());
        #endregion
    }
}