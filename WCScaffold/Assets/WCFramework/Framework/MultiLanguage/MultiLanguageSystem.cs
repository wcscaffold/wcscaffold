﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;
public enum LanguageType
{
    Chinese,
    English
}

public interface IMultiLanguageSystem : ISystem
{
    /// <summary>
    /// 设置语种
    /// </summary>
    void SetLanguage(LanguageType languageType);

    /// <summary>
    /// 获取当前语言下的文本
    /// </summary>
    string GetText(string languageCode);
}

public class MultiLanguageSystem : AbstractSystem, IMultiLanguageSystem
{
    private LanguageType languageType = LanguageType.Chinese;

    private Dictionary<string, MultiLanguageItem> multiLanguageDic = new Dictionary<string, MultiLanguageItem>();

    protected override void OnInit()
    {
        MultiLanguageManager multiLanguageManager = Resources.Load<MultiLanguageManager>("MultiLanguageItemAsset");
        //TODO 等有数据再解开
        foreach (MultiLanguageItem multiLanguageItem in multiLanguageManager.dataArray)
        {
            multiLanguageDic.Add(multiLanguageItem.languageCode, multiLanguageItem);
        }
    }


    public string GetText(string languageCode)
    {
        multiLanguageDic.TryGetValue(languageCode, out MultiLanguageItem multiLanguageItem);
        if (multiLanguageItem != null)
        {
            switch (languageType)
            {
                case LanguageType.Chinese:
                    return multiLanguageItem.Chinese;
                default:
                    return multiLanguageItem.English;
            }
        }
        else
        {
            //Debug.LogError("找不到当前语言编码所对应的文本，请检查 ：" + languageCode);
            return languageCode;
        }
    }

    public void SetLanguage(LanguageType languageType)
    {
        this.languageType = languageType;
    }
}
