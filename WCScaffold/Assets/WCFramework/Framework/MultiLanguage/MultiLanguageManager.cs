﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class BaseItem { }

[System.Serializable]
public class MultiLanguageManager : ScriptableObject
{
    public MultiLanguageItem[] dataArray;
}

[System.Serializable]
public class MultiLanguageItem : BaseItem
{
    public string languageCode;
    public string chinese;
    public string english;

    public string LanguageCode { get { return languageCode; } set { languageCode = value; } }
    public string Chinese { get { return chinese; } set { chinese = value; } }
    public string English { get { return english; } set { english = value; } }

}