﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoginForm : BaseUIForm
{
    /// <summary>
    /// 开始虚拟仿真的按钮
    /// </summary>
    private WCBaseButton startVirtualSimulationButton;

    private InputField userName;
    private InputField password;

    private const string VERSION = "1.2.1";

    private void Awake()
    {
        Init(UIFormShowMode.HideOther, UIFormType.Fixed);

        startVirtualSimulationButton = transform.Find("BackGround/Image/StartButton").GetComponent<WCBaseButton>();
        startVirtualSimulationButton.onValueChanged = StartVirtualSimulation;

        userName = transform.Find("BackGround/Image/InputFieldGroup/userName").GetComponent<InputField>();
        userName.characterLimit = 20;
        password = transform.Find("BackGround/Image/InputFieldGroup/password").GetComponent<InputField>();
        password.characterLimit = 20;
    }
    private void Start()
    {
        //DataMutualInfo.CheckVersion((originalRequest, response) =>
        //{
        //    if (response.DataAsText != VERSION)
        //    {
        //        tips.gameObject.SetActive(true);
        //    }
        //    else
        //    {
        //        tips.gameObject.SetActive(false);
        //    }
        //});
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            StartVirtualSimulation(null, true);
        }
    }
    /// <summary>
    /// 开始虚拟仿真
    /// </summary>
    private void StartVirtualSimulation(GameObject gameObject, bool isOpen)
    {
        CloseUIForm<LoginForm>(gameObject, isOpen);


        //DataMutualInfo.SendSInfoLoginInfo(userName.text, password.text, (originalRequest, response) =>
        //{
        //    if (response != null)
        //    {
        //        if (response.DataAsText == "0")
        //        {
        //            this.GetModel<IGameModel>().SceneName.Value = GameSceneName.ProjectIntroduction.ToString();
        //        }
        //        else
        //        {
        //            password.text = "";
        //            errorTip.gameObject.SetActive(true);

        //            if (hideText != null)
        //            {
        //                hideText.Restart();
        //            }
        //            else
        //            {
        //                errorTip.color = new Color(errorTip.color.r, errorTip.color.g, errorTip.color.b, 1);
        //                hideText = ButtonFade(errorTip);
        //                hideText.OnStepComplete(() => { hideText = null; });
        //            }
        //        }
        //    }
        //});
    }
}
