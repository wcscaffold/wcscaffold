﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;
using System.Runtime.InteropServices;
using System;

public class GlobalToolbarForm : MonoBehaviour, IController
{

    private void Awake()
    {
        transform.Find("BG/colseButton").GetComponent<WCBaseButton>().onValueChanged = ExitGame;
        transform.Find("BG/gameSetUpButton").GetComponent<WCBaseButton>().onValueChanged = OpenGameSetUpForm;
        transform.Find("BG/paintBrushButton").GetComponent<WCBaseButton>().onValueChanged = OpenPaintBrushForm;

        transform.Find("BG/minimizeButton").GetComponent<WCBaseButton>().onValueChanged = OnClickMinimize;
        transform.Find("BG/windowButton").GetComponent<WCBaseButton>().onValueChanged = OnClickRestore;

    }

    private void OnDisable()
    {
        Screen.fullScreen = true;  //设置成全屏
    }

    private void OpenGameSetUpForm(GameObject obj, bool isOpen)
    {
        this.GetSystem<IUIManagerSystem>().ShowUIForm<GameSetUpForm>(SceneName.Common);
    }

    private void OpenPaintBrushForm(GameObject obj, bool isOpen)
    {
        this.GetSystem<IUIManagerSystem>().ShowUIForm<PaintBrushForm>(SceneName.Common);
    }

    /// <summary>
    /// 退出应用
    /// </summary>
    private void ExitGame(GameObject gameObject, bool isOpen)
    {
        StartCoroutine(Exit());
    }

    IEnumerator Exit()
    {
        yield return new WaitForSeconds(0.1f);
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }

    public IArchitecture GetArchitecture()
    {
        return GameCneter.Interface;
    }

    [DllImport("user32.dll")]
    public static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

    [DllImport("user32.dll")]
    static extern IntPtr GetForegroundWindow();

    const int SW_SHOWMINIMIZED = 2; //{最小化, 激活}
                                    //const int SW_SHOWMAXIMIZED = 3;//最大化
                                    //const int SW_SHOWRESTORE = 1;//还原
                                    //public const int SW_SHOWNOACTIVATE = 4;
    public void OnClickMinimize(GameObject obj, bool isOpen)
    {
        //最小化 
        ShowWindow(GetForegroundWindow(), SW_SHOWMINIMIZED);
    }



    public void OnClickRestore(GameObject obj, bool isOpen)
    {
        // ShowWindow(GetForegroundWindow(), SW_SHOWNOACTIVATE);
        if (isOpen)
        {
            //获取设置当前屏幕分辩率 
            Resolution[] resolutions = Screen.resolutions;
            //设置当前分辨率 
            Screen.SetResolution(resolutions[resolutions.Length / 2].width, resolutions[resolutions.Length / 2].height, true);
            Screen.fullScreen = false;  //设置成全屏
        }
        else
        {
            Screen.fullScreen = true;  //设置成全屏
        }

    }
}
