﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class PaintBrushForm : BaseUIForm
{
    private PaintBrush paintBrush;
    private Transform colorButtonGroup;
    private Transform brushSizeButtonGroup;
    private void Awake()
    {
        Init(UIFormShowMode.ReverseChange, UIFormType.PopUp, UIFormLucencyType.Lucency);
    }

    protected override void Init()
    {
        paintBrush = transform.Find("myCanvas").GetComponent<PaintBrush>();

        transform.Find("BG/deleteButton").GetComponent<WCBaseButton>().onValueChanged = ClearPaintBrush;
        transform.Find("BG/saveButton").GetComponent<WCBaseButton>().onValueChanged = FullScreenShot;
        transform.Find("BG/closeButton").GetComponent<WCBaseButton>().onValueChanged = CloseUIForm<PaintBrushForm>;

        colorButtonGroup = transform.Find("BG/colorButtonGroup");

        for (int i = 0; i < colorButtonGroup.childCount; i++)
        {
            colorButtonGroup.GetChild(i).GetComponent<WCBaseButton>().onValueChanged = SetBrushColor;
        }

        brushSizeButtonGroup = transform.Find("BG/brushSizeButtonGroup");

        for (int i = 0; i < brushSizeButtonGroup.childCount; i++)
        {
            brushSizeButtonGroup.GetChild(i).GetComponent<WCBaseButton>().onValueChanged = SetBrushSize;
        }
    }

    private void OnEnable()
    {
        paintBrush.inital();
        colorButtonGroup.GetChild(0).GetComponent<WCBaseButton>().IsOn = true;
        brushSizeButtonGroup.GetChild(0).GetComponent<WCBaseButton>().IsOn = true;
    }

    private void ClearPaintBrush(GameObject obj, bool isOpen)
    {
        paintBrush.Clear();
    }

    private void SetBrushColor(GameObject obj, bool isOpen)
    {
        Debug.Log(obj.name);
        if (isOpen)
        {
            switch (obj.name)
            {
                case "red":
                    paintBrush.SelectColor(Color.red);
                    break;
                case "blue":
                    paintBrush.SelectColor(Color.blue);
                    break;
                case "black":
                    paintBrush.SelectColor(Color.black);
                    break;
                case "green":
                    paintBrush.SelectColor(Color.green);
                    break;
                case "yellow":
                    paintBrush.SelectColor(Color.yellow);
                    break;
            }
        }
    }

    private void SetBrushSize(GameObject obj, bool isOpen)
    {
        if (isOpen)
        {
            paintBrush.SetLineWidth(int.Parse(obj.name));
        }
    }


    private void FullScreenShot(GameObject obj, bool isOpen)
    {
        OpenFileName openFileName = new OpenFileName();
        openFileName.structSize = Marshal.SizeOf(openFileName);
        // openFileName.filter = "图片文件(*jpg;*.png)\0*.jpg;*.png\0视频文件(*.mp4)\0*.mp4\0\0";
        openFileName.filter = "图片文件(*.png)\0*.png";

        openFileName.file = new string(new char[256]);
        openFileName.maxFile = openFileName.file.Length;
        openFileName.fileTitle = new string(new char[64]);
        openFileName.maxFileTitle = openFileName.fileTitle.Length;
        openFileName.initialDir = Application.streamingAssetsPath.Replace('/', '\\');//默认路径
        openFileName.title = "窗口标题";
        openFileName.flags = 0x00080000 | 0x00001000 | 0x00000800 | 0x00000008;
        LocalDialog.GetSaveFileName(openFileName);
        ScreenCapture.CaptureScreenshot(openFileName.file + ".png");
    }

}
