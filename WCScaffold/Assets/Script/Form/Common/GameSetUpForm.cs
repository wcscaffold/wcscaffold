﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FrameworkDesign;

public class GameSetUpForm : BaseUIForm
{
    private Slider mainVolumeSlider;
    private Slider bgVolumeSlider;
    private Slider clickVolumeSlider;
    private Slider explainVolumeSlider;

    public static float mainVolumeCount = 1;
    public static float bgVolumeCount = 1;
    public static float clickVolumeCount = 1;
    public static float explainVolumeCount = 1;

    private void Awake()
    {
        Init(UIFormShowMode.Normal, UIFormType.Normal);
    }

    protected override void Init()
    {
        mainVolumeSlider = transform.Find("bg/mainVolume").GetComponent<Slider>();
        mainVolumeSlider.value = 1;
        mainVolumeSlider.onValueChanged.AddListener(SetMainVloume);

        bgVolumeSlider = transform.Find("bg/bgVolume").GetComponent<Slider>();
        bgVolumeSlider.value = 1;
        bgVolumeSlider.onValueChanged.AddListener(SetBGVloume);

        clickVolumeSlider = transform.Find("bg/clickVolume").GetComponent<Slider>();
        clickVolumeSlider.value = 1;
        clickVolumeSlider.onValueChanged.AddListener(SetClickVloume);

        explainVolumeSlider = transform.Find("bg/explainVolume").GetComponent<Slider>();
        explainVolumeSlider.value = 1;
        explainVolumeSlider.onValueChanged.AddListener(SetExplainVloume);
    }

    private void SetMainVloume(float volume)
    {
        mainVolumeCount = volume;
        this.SendEvent(new ChangeSoundVolumeEvent { soundType = SoundType.None });
    }

    private void SetBGVloume(float volume)
    {
        bgVolumeCount = volume;
        this.SendEvent(new ChangeSoundVolumeEvent { soundType = SoundType.OnBG });
    }

    private void SetClickVloume(float volume)
    {
        clickVolumeCount = volume;
    }

    private void SetExplainVloume(float volume)
    {
        explainVolumeCount = volume;
    }


    private void SetQuality(GameObject obj, bool isOpen)
    {
        string qualityName = obj.name;
        if (isOpen)
        {
            switch (qualityName)
            {
                case "1":
                    QualitySettings.SetQualityLevel(1, true);
                    break;
                case "2":
                    QualitySettings.SetQualityLevel(3, true);
                    break;
                case "3":
                    QualitySettings.SetQualityLevel(5, true);
                    break;
            }
        }
    }
}
