﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;
using UnityEngine.EventSystems;
using System;
using UnityEngine.UI;

public class DrawLineController : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    //主要用于渲染的Texture2D
    private Texture2D mainUseTexture;

    //绘画得到的图片
    private RawImage paintCanvas;

    //可供选择的颜色 可随便自定义
    public Color[] checkColor;

    //当前选择的画笔颜色
    private Color currentColor;

    private Color32[] allColor;

    public int lineWidth = 10;

    private void Start()
    {
        inital();

    }

    /// <summary>
    /// 初始化
    /// </summary>
    public void inital()
    {
        paintCanvas = GetComponent<RawImage>();
        mainUseTexture = new Texture2D(960, 540);
        paintCanvas.texture = mainUseTexture;
        Clear();
        SelectColor(checkColor[1]); //选择一个默认颜色

        allColor = mainUseTexture.GetPixels32();
    }

    /// <summary>
    /// 清楚画板所有的像素点颜色 测试用 可忽略
    /// </summary>
    public void Clear()
    {
        for (int i = 0; i < mainUseTexture.width; i++)
        {
            for (int y = 0; y < mainUseTexture.height; y++)
            {
                mainUseTexture.SetPixel(i, y, new Color(1, 1, 1, 0));
            }
        }
        mainUseTexture.Apply();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            Array.Clear(allColor, 0, allColor.Length);
            mainUseTexture.SetPixels32(allColor);
            mainUseTexture.Apply();
        }


    }

    /// <summary>
    /// 设置画笔颜色 挂在游戏物体上事件
    /// </summary>
    /// <param name="chooseColor"></param>
    public void SelectColor(Color chooseColor)
    {
        currentColor = chooseColor;
    }

    private Vector2 previous_drag_position = Vector2.zero;

    /// <summary>
    /// 鼠标按下时绘制
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerDown(PointerEventData eventData)
    {
        Vector2 localPos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            transform.parent.transform as RectTransform,
            eventData.position,
            eventData.pressEventCamera,
            out localPos
            );

        var _x = 480.0f + localPos.x / 2;
        var _y = 270.0f + localPos.y / 2;
        MarkPixelsToColour(new Vector2(_x, _y));
        previous_drag_position = new Vector2(_x, _y);

        mainUseTexture.SetPixels32(allColor);
        mainUseTexture.Apply();
    }

    /// <summary>
    /// 鼠标移动时绘制
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrag(PointerEventData eventData)
    {
        Vector2 localPos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            transform.parent.transform as RectTransform,
            eventData.position,
            eventData.pressEventCamera,
            out localPos
            );

        var _x = 480.0f + localPos.x / 2;
        var _y = 270.0f + localPos.y / 2;
        ColourBetween(previous_drag_position, new Vector2(_x, _y));
        previous_drag_position = new Vector2(_x, _y);
        mainUseTexture.SetPixels32(allColor);
        mainUseTexture.Apply();
    }

    private void ColourBetween(Vector2 start_point, Vector2 end_point)
    {
        float distance = Vector2.Distance(start_point, end_point);
        Vector2 direction = (start_point - end_point).normalized;

        Vector2 cur_position = start_point;
        float lerp_steps = 1 / distance;

        for (float lerp = 0; lerp <= 1; lerp += lerp_steps)
        {
            cur_position = Vector2.Lerp(start_point, end_point, lerp);
            MarkPixelsToColour(cur_position);
        }
    }

    private void MarkPixelsToColour(Vector2 center_pixel)
    {
        int center_x = (int)center_pixel.x;
        int center_y = (int)center_pixel.y;

        for (int x = center_x - lineWidth; x <= center_x + lineWidth; x++)
        {
            if (x >= (int)mainUseTexture.width || x < 0)
                continue;

            for (int y = center_y - lineWidth; y <= center_y + lineWidth; y++)
            {
                MarkPixelToChange(x, y);
            }
        }
    }

    /// <summary>
    /// 将坐标转换成图片所有颜色的索引，并更改其颜色
    /// </summary>
    private void MarkPixelToChange(int x, int y)
    {
        int array_pos = y * (int)mainUseTexture.width + x;

        if (array_pos > allColor.Length || array_pos < 0)
            return;

        allColor[array_pos] = currentColor;
    }
}
