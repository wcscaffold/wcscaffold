﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrameworkDesign;
using System.Runtime.InteropServices;
using System.IO;
using System;

public class GameStart : MonoBehaviour, IController
{

    public IArchitecture GetArchitecture()
    {
        return GameCneter.Interface;
    }

    private void Awake()
    {
        //this.GetModel<IGameModel>().SceneName.Value = GameSceneName.Login.ToString();
        //this.GetSystem<IUIManagerSystem>().ShowUIForm<GameSetUpForm>(SceneName.Common);
        StartCoroutine(PlayBG());
        this.GetSystem<IUIManagerSystem>().ShowUIForm<LoginForm>(SceneName.HomePage);
    }

    IEnumerator PlayBG()
    {
        yield return new WaitForSeconds(1);
        this.SendEvent(new SoundInfoEvent { soundType = SoundType.OnBG, soundName = SoundName.BG, soundVolume = 1 });
    }
}
